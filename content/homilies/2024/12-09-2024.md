+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "advent", "2024"]
date = "2024-12-09"
description = "Immaculate Conception of the Blessed Virgin Mary, Year C"
searchdesc = "Mary was full of grace, Jesus graced the world by his sacrifice, and now that grace comes to us through the sacraments."
image = "/img/homilies/immaculate-conception-bvm2.jpg"
featured = "immaculate-conception-bvm2.jpg"
featuredalt = "Immaculate Conception of the Blessed Virgin Mary"
featuredpath = "/img/homilies/"
linktitle = ""
title = "O Mary Conceived Without Sin, Pray For Us Who Have Recourse to Thee"
type = "post"
format = "homily"

+++

<!-- {{< audio "/audio/homily-second-sunday-advent-year-c.mp3" >}}  
*(Audio recorded live, 5 December 2021)* -->

# Readings:
## Gn. 3:9-15, 20; Ps. 98; Eph. 1:3-6, 11-12; Lk. 1:26-38

Today we celebrate the Solemnity of the Immaculate Conception of the Blessed Virgin Mary. Pope Pius IX instituted this Solemnity in honor of the Blessed Mother in 1854. Mary had long since been venerated as the Theotokos, or Mother of God, and the theology of Mary developed alongside the theology of Christ. It is through Mary that Jesus took human form, but how is it that Jesus was born without sin? The logical conclusion is that in order for Jesus to be born without sin, his mother would also have to be without sin. This doctrine of the Immaculate Conception developed throughout the centuries until Pio Nono declared it formally in 1854.

God chose to make his entry into this world through a special womb. And as we heard in the Gospel, the angel Gabriel addresses Mary as “full of grace.” Luke tells us, “Mary was greatly troubled at what was said and pondered what sort of greeting this might be.” How is it an angel of the Lord would appear to a lowly handmaid and greet her with such respect and admiration? To be full of grace must mean that she is highly favored, even above the angels! Clearly God had a very important plan for her life.

Adam and Eve represent the failure to keep God’s command, while Mary’s ‘yes’ to God shows a great reversal. While all of mankind has been tainted by the sin of our first parents, Mary introduces a new paradigm by her fiat, saying, “May it be done to me according to your word.” By saying ‘yes’ to God, she has said ‘no’ to sin.

The dogma of the Immaculate Conception teaches that Mary was conceived without sin. That is why the angel greets her as “full of grace.” It was by the grace of God she was conceived in such a way so as to be completely unstained by sin. And that she gave birth to Jesus shows her cooperation with God’s grace to bring about the salvation of humanity.

So, as we celebrate this great feast of Mary’s Immaculate Conception, her freedom from sin, let us remember our own freedom from sin through the blood of Christ. He paid the ultimate cost for us to be free, not so we can do whatever we want when we want, but so we can enjoy true freedom as adopted sons and daughters of the Father. As St. Paul says, “In love he destined us for adoption to himself through Jesus Christ, in accord with the favor of his will, for the praise of the glory of his grace that he granted us in the beloved.” Mary was filled with this grace, Jesus graced the world by his sacrifice, and now that grace comes to us through the sacraments. May we unite our hearts with his as we honor the Blessed Mother today, and may her ‘yes’ to the Word of God be our example, not just during Advent, but for years to come.

---