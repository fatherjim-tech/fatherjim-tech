+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "christmas", "2024"]
date = "2024-12-24"
description = "The Nativity of the Lord, Year C (Vigil)"
searchdesc = "May the gift of God’s grace in the Most Blessed Sacrament come to dwell in each of us as we seek him with all our heart."
image = "/img/homilies/nativity-scene4.jpg"
featured = "nativity-scene4.jpg"
featuredalt = "The Holy Family in the stable greeted by shepherds."
featuredpath = "/img/homilies/"
linktitle = ""
title = "God, according to his promise, has brought to Israel a savior, Jesus"
type = "post"
format = "homily"

+++

<!-- {{< audio "/audio/homily-second-sunday-advent-year-c.mp3" >}}  
*(Audio recorded live, 5 December 2021)* -->

# Readings:
## Is. 62:1-5; Ps. 89; Acts 13:16-17, 22-25; Matt. 1:18-25

Good evening and Merry Christmas! It’s hard to believe this is our second Christmas together. Where has the time gone? I am happy to see so many of our families here. Wasn’t that beautiful having the children process with the baby Jesus? By now most of you know how much I love Church tradition. I think I get it from my mother, who is one of the most sentimental people I know. Just the other day I noticed she had a collection of lids for porcelain tea pots. Being the nudge that I am, I asked her, “What are you doing with all these lids?” She said, “The pots broke, so I was saving them in case I came across the matching bottom.” Now, if I were a good son, I would take a quick picture of those lids and try and find a bottom for her for Christmas. But, I’m a bit more practical than that. And so, I got her stainless steel instead. She’ll have a hard time breaking the bottom on that one.

So, here we are, another year, another Christmas. So much has happened this year, I have to admit, I found myself being tempted to distraction constantly. How many of you feel this way? We live at a time when news is practically instantaneous. Sometimes it seems we are hearing the news from half way around the world even before the people who live there. Information flies so quickly, and there is also a temptation to keep up. Along with those distractions, we have our professional responsibilities, our community responsibilities, and most importantly, our family.

Our families are the most important people in our lives. Are we spending enough time with these people? Are outside distractions keeping us from being with our families? Beyond that, are we being tempted to neglect our relationship with God? Often, when we are distracted, God is the first one we ignore. Why is that? Certainly, God does not ignore us. To gain some perspective on how much God is concerned with us, if God were to stop thinking about us, we would cease to be. That is why St. Paul says, “In him we live and move and have our being” (Acts. 17:28). Our life is a share in the life of God, and this becomes all the more clear for us at Christmas when we celebrate the way in which God chose to share in our humanity by becoming one with us in Jesus.

Our readings today show us the many ways in which God prepared the people of Israel for his arrival. God is very polite. Just as we would send out save-the-date cards for a special occasion, or mail wedding invitations, God spoke through his holy prophets, who announced his coming. The prophet Isaiah says, “For the LORD delights in you and makes your land his spouse.” In other words, God loves Israel like a husband loves his bride. Our psalm recalls for us the Davidic Covenant, which the Lord made with King David, saying, “I have sworn to David my servant: Forever will I confirm your posterity and establish your throne for all generations.” So, the kingship of David will carry on through an heir.

Our Gospel gives us the genealogy of Jesus, which traces Jesus’ ancestral roots to David, fulfilling the Lord’s promise, and continues all the way back to Abraham, our father in faith. The faith of Abraham is the same faith we share today, namely, faith in the One True God, who keeps his promises throughout the ages. To Abraham, God promised to make his descendants as numerous as the stars in the sky. How many people believe that Jesus is the Son of God? How many believed that over the centuries? How many will believe in the years to come? St. Peter tells us, “with the Lord one day is like a thousand years and a thousand years like one day” (2 Pt. 3:8). In other words, God willed that for all time, his people would come to know him, to love him, and to serve him in this life, so they may be happy with him forever in heaven.

At Christmas, we experience a foretaste of heaven. Let us reflect on the way in which our Blessed Lord was born. Jesus himself said, “I have come to bring not peace but the sword” (Mt. 10:34b). The Incarnation of Jesus was no simple matter. Mary was a consecrated virgin. Even though she was betrothed to Joseph, they would have remained chaste because her consecration came before the marriage. This was not unheard of in those days. Once a prophet was commissioned, they would remain chaste. It was the same with Jesus’ apostles. They were given a greater commission, to speak the good news of God. Joseph, assuming the worst, tries to divorce Mary quietly. He does this to save her from being shamed, which tells us something about his character: He was a righteous man. But, the angel speaks to him in a dream and tells him that he, too, is being called to a greater commission, for it was by an act of God that Mary conceived, and that the son she bears is to be named Jesus, which means God-saves. How’s that for a distraction? If I had a dream like that, I don’t think I would be comfortable going to sleep. This is a tall order. Of course, Matthew shows us how Joseph is in the line of David, and his marriage to Mary establishes Jesus as the Son of David.

St. Paul reminds us of Jesus’ relationship to King David, saying, “From this man’s descendants God, according to his promise, has brought to Israel a savior, Jesus.” The Nativity of Jesus also fulfills the prophecy of Isaiah, who says, “Behold, the virgin shall conceive and bear a son, and they shall name him Emmanuel, which means ‘God is with us.’” Notice the passage says, “the virgin” not “a virgin.” Mary is the Virgin of virgins, the most pure, immaculately conceived, full of grace. It is through her that Jesus may be called Emmanuel, God-with-us.

Where does all this fit in to our world of heavy distraction? For starters, it should remind us that God is always with us, even though we may sometimes not be with him. Next week is the beginning of a new year. As Catholics we begin the new year with a Holy Day of Obligation: The Solemnity of the Blessed Virgin Mary, the Mother of God. One of the ways we deal with distraction in the Church is to fill our time with holy distractions, to come to Mass on Sundays and Holy Days, not just Christmas and Easter, but all the days. When we lead this life of faith, we find that our Church distractions are not so much distractions as they are a way of life, our Catholic identity. Jesus says, “I came so that they might have life and have it more abundantly” (Jn. 10:10b). How can we have this abundance if we are not in Church to receive it? Look around. This is how the Church should look every Sunday.

There is a saying, “The Church is not a hotel for saints; it is a hospital for sinners.” In his writings, St. Augustine often spoke about the Church’s mission to guide sinners towards repentance and reconciliation with God. The Church is holy, but it is also composed of individuals who are imperfect and in need of continual conversion. There is a reason the angel told Mary and Joseph that the child to be born will be named Jesus, for God-saves. How is Jesus saving us today? Do we listen to his voice, or do we let distractions drown him out?

When it comes to overcoming distraction, all it takes is conscious effort. For instance, if we realize we are spending too much time being distracted, we can make a conscious effort to limit our time with that distraction. We don’t have to be extremists. We don’t have to delete all the apps on our phone just so we don’t use them, but we can consciously choose not to waste time. Finally, if we fill up our time with holy distractions: Coming to Mass, praying the Rosary, going on religious retreats, joining local prayer groups, that time is never wasted, because we give our time to God, just as he has given us this time to live.

And so, as we celebrate the Nativity of our Blessed Lord, who chose to make his home with us, let us be ever mindful of his presence among us, especially in the Most Blessed Sacrament of the Altar. Jesus assured his disciples that he would remain with them always, even to the end of the age. In a few moments, Jesus will become present to us in the Eucharist. May this gift of God’s grace come to dwell in each of us as we seek him with all our heart. Have a most blessed and a very merry Christmas.

---