+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "advent", "2024"]
date = "2024-12-15"
description = "Third Sunday of Advent, Year C"
searchdesc = "This celebration of Mass is a foretaste of what God has in store for each of us, namely, a perfect union with God and one another."
image = "/img/homilies/advent-candles-gaudete-sunday.jpg"
featured = "advent-candles-gaudete-sunday.jpg"
featuredalt = "Advent candles - two purple and one pink are lit."
featuredpath = "/img/homilies/"
linktitle = ""
title = "Gaudete Sunday - Rejoice in the Lord!"
type = "post"
format = "homily"

+++

<!-- {{< audio "/audio/homily-second-sunday-advent-year-c.mp3" >}}  
*(Audio recorded live, 5 December 2021)* -->

# Readings:
## Zep. 3:14-18a; Is. 12:2-6; Phil. 4:4-7; Lk. 3:10-18

Rejoice! Gaudete! Rejoice! Cry out with joy and gladness, for the Lord, our God, is in our midst! Today, on this third Sunday of Advent, we celebrate Gaudete Sunday, which marks the halfway point of Advent and focuses our attention on Christian joy. Last week, our Gospel narrative introduced John the Baptist, who came proclaiming a baptism of repentance, but today’s Gospel emphasizes Christ, who will baptize with the Holy Spirit and fire. That same Spirit is present with us today as we celebrate this Mass in anticipation of the celebration of Jesus’ arrival on Christmas Day.

Our Gospel narrative today expands on John the Baptist, highlighting the people who followed him and some of his teachings. Most importantly, we learn of John’s humility as the front-runner of Christ. I would like to give you a little background on who John was. John the Baptist, as we heard in the infancy narrative on the Feast of the Immaculate Conception, was the son of Mary’s cousin, Elizabeth, who was the wife of Zechariah, one of the priests of the Temple. John would have grown up steeped in the Jewish faith and would have learned first-hand from his father. His father would likely have shared with him the revelation he received from the angel Gabriel who told him that his son would be a voice crying out in the desert: Prepare the way of the Lord. And so, when he was of age, John went out into the desert and began to proclaim a baptism of repentance. Those who heard him preaching would have been drawn by his message and the way he lived his life. He lived simply, but had a powerful message. Even King Herod liked to listen to John. That is the kind of gift John had as the forerunner of the Lord.

St. Luke is an expert at painting a picture with words. In our Gospel today, we see three different types of people: Soldiers, tax collectors, and large crowds of people. These groups represent people of differing status. The soldiers, who were likely Roman or even non-Roman auxiliary troops, come to ask John what they should do. He tells them they should not extort anyone and be satisfied with their wages. This is a moral teaching addressing their conduct and ethical responsibilities. They are encouraged to act justly and with integrity, which would have been contrary to the prevailing norms of seeking power and authority. Such ambition has no place in the kingdom of God.

The next group are tax collectors, who also ask what they should do. John tells them to stop collecting more than what they should. Tax collectors would often charge more for the taxes and enrich themselves. They were dishonest, which is part of the reason they were despised by the people. Still, they came to John the Baptist, which is signifies the potential for repentance and transformation. Last, we have the crowds. Many times throughout gospels, the crowds are like proxy characters. They tend to represent everyone. And the crowds ask, “What should we do?” John says, whoever has two coats should share with the person who has none, and whoever has food should do likewise. Three different groups, three different teachings, but great advice for all who hear.

When the people heard John’s teaching, they were filled with expectation and pondered whether John might be the Christ, but this is where John’s humility kicks in. Even though his father was a priest; even though he preached a great message; even though he had a huge following, he knew the prophecy of his own father on the day he was born, who said he will go before the Lord to prepare his way, to give his people knowledge of salvation by the forgiveness of their sins. And so, John the Baptist points not to himself, but to Christ, saying, “one mightier than I is coming. I am not worthy to loosen the thongs of his sandals.” In another Gospel he says, “He must increase, I must decrease” (Jn. 3:30).

So, what about us? Which group of people are we from today’s Gospel? Are we the soldiers, the tax collectors, or are we the crowd? Either way, we are all called to humble ourselves so that Jesus may increase in us—to put aside earthly things so that we may be blessed with the things of heaven. The more we repent, the more we turn away from temptation and sin, the more space we create for God in our hearts.

It is our responsibility to help one another grow in holiness and move towards our ultimate destination, which is heaven. This celebration today is a foretaste of what God has in store for each of us, namely, a perfect union with God and one another. May we all allow Christ to increase in us daily. And let us rejoice, for today the Lord is present in our midst. Rejoice! Gaudete! Rejoice!

---