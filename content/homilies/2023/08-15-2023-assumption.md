+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["solemnities", "ordinary-time", "2023"]
date = "2023-08-15"
description = "Solemnity of the Assumption of the Blessed Virgin Mary, Year A"
searchdesc = "From the beginning of his life to the end, Mary was ever present to our blessed Lord, as a candle to a flame. And as her candle was extinguished, she never stopped guiding us to Christ, who came to take her to himself."
image = "/img/homilies/mary-assumption-2.jpg"
featured = "mary-assumption-2.jpg"
featuredalt = "Mary being assumed into heaven with angels"
featuredpath = "/img/homilies/"
linktitle = ""
title = "Mary Reflects the Light of Christ"
type = "post"
format = "homily"

+++

# Readings:
## Rv. 11:19A; 12:1-6A, 10AB ; Ps. 45; 1 Cor. 15:20-27; Lk. 1:39-56

Today, we celebrate the Solemnity of the Assumption of the Blessed Virgin Mary. This solemnity dates back to at least the sixth century when the Church celebrated Mary’s dormition, or her going to sleep. But, in the Church’s Tradition, her sleep was always followed by her rising and being taken into heaven. We call Mary’s entry into heaven her assumption, because she was assumed, body and soul, into the heaven. Just as Jesus rose from the dead, so too was Mary awakened from her sleep.

The Church kept the feast of Mary’s Dormition for centuries. It was not until November 1, 1950, when Pope Pius XII, taking note of the popular devotion to the Blessed Mother throughout the ages, declared the Assumption of Mary to be an official dogma of the Catholic Church. While this dogma was challenged by many Protestants, it was the descendant of a long line of Swiss Protestant ministers, the psychologist Carl Jung, who acknowledged the effectiveness of Catholic devotion to the Blessed Mother, who made approaching the Divine gentler and more accessible.

So much of what the Church has said about Mary begins with what was first said about Christ. We call the study of Christ and his Person “Christology.” This branch of theology is a reflection on who Jesus is. But, following closely after the study of Jesus often comes the study of who Mary is. We call this branch, “Mariology.” So, the Church’s reflection has always begun with Christ, and then extended its reflection to Mary.

We hear in the gospel how a woman says to Jesus, “Blessed is the womb that carried you and the breasts at which you nursed” (Lk. 11:27). This passage praises Jesus, but also his mother, who carried him, nursed him, and helped prepare him for his mission. And Jesus, recognizing how devout his mother was, replies, “Rather, blessed are those who hear the word of God and observe it” (Lk. 11:28). In other words, those who, like Mary, hear the word of God and observe it, they are blessed.

When Mary encounters the angel Gabriel, he told her that she would conceive and bear a son and he will be great and be called Son of the Most High. She knew that this was a tall order, but she said, “Let it be done to me according to your word.” Then she travels to her cousin Elizabeth’s house to discover she also is pregnant, just as the angel said, for nothing is impossible for God. At that moment, she had confirmation of the seed of faith already planted in her heart. This is why she bursts out in song with her Magnificat, saying, “My soul proclaims the greatness of the Lord, my spirit rejoices in God my Savior for he has looked with favor on his lowly servant.” Here she was, a young woman blessed to carry the Son of God. Even her cousin Elizabeth exclaims, “Blessed are you among women, and blessed is the fruit of your womb.” These words are part of the Hail Mary prayer, and they remind us of how blessed Mary truly is.

Mary always reflects the light of Christ. We see how she reflects that light in her fiat, in the Magnificat, and in the quiet way in which she follows Christ. Indeed, Mary is truly the first disciple of Jesus—it was she who pointed out that they had no wine at the wedding in Cana. And Mary was among the last of the disciples standing at the foot of the cross. From the beginning of his life to the end, Mary was ever present to our blessed Lord, as a candle to a flame. And as her candle was extinguished, she never stopped guiding us to Christ, who came to take her to himself.

The Book of Revelation speaks of the Ark of the covenant in heaven. Mary is that Ark, because just as the first Ark held the tablets of the Ten Commandments, that is, the Word of God, so too did Mary hold the Jesus, the Word of God, in the Ark of her womb. John describes her as a woman clothed by the Sun, with a crown of twelve stars. She is depicted as radiant because she has been filled by the light of Christ, and she is surrounded by twelve stars, that is, twelve apostles, those entrusted with carrying his message to the ends of the earth. This is a heavenly image of Mary, who carried out the mission entrusted to her and has been crowned Queen of Heaven.

And so, as we honor the Blessed Virgin Mary on this wonderful feast of her Assumption into Heaven, may our coming together today and our sharing in this holy meal, strengthen our faith and hope in the resurrection so that our communion will be nourishment not just for today, but for eternity.

---
