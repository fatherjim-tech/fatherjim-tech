+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "2023"]
date = "2023-09-09"
description = "Twenty-third Sunday in Ordinary Time, Year A"
searchdesc = "By nature of our baptism, all the faithful have a share in the threefold mission of Jesus, who is priest, prophet, and king. And since the role of a prophet is to teach, whenever we teach one another, as a father his son, a mother her daughter, a catechist their student, a priest his congregation, we are exercising the prophetic ministry of Christ."
image = "/img/homilies/jesus-teaching.jpg"
featured = "jesus-teaching.jpg"
featuredalt = "Jesus teaching his disciples"
featuredpath = "/img/homilies/"
linktitle = ""
title = "We Are Called to Admonish Sinners"
type = "post"
format = "homily"

+++

# Readings:
## Ez. 33:7-9; Ps. 95; Rm. 13:8-10; Mt. 18:15-20

Today, Jesus teaches his disciples about the importance of admonishing the sinner. Those who remember their eighth grade catechism will remember the Corporal and Spiritual Works of Mercy. Among the Spiritual Works is the imperative to admonish the sinner. What does it mean to admonish? To admonish someone is to counsel them against something to be avoided or warn against something that is dangerous. So, let us take a deeper look at this important spiritual work of mercy.

In the Gospel of Luke, Jesus asks, “What father among you would hand his son a snake when he asks for a fish? Or hand him a scorpion when he asks for an egg?” (Lk. 11:11-12). In other words, the role of a parent is not to lead their child into danger, but rather to protect them, to guard them, to love them. Just like today a parent would never tell their child to go play in traffic, rather, they would warn their child of the danger of crossing the road, how to look both ways, and to always watch for cars. In a similar way, the Ten Commandments teach us to avoid sin, the wages of which, as St. Paul tells us, is death. So, just as one can be physically hurt by failing to observe the warnings of crossing the road, so too, can one be spiritually hurt by failing to observe the warnings of God.

If parents are the ones typically charged with the responsibility to admonish and warn their children about the dangers of this world, who then are the ones responsible for admonishing or warning us about the spiritual pitfalls? The prophet Ezekiel was a priest in Jerusalem during the Babylonian Exile. The Lord appointed him as watchman for the house of Israel. A watchman is one who looks out beyond the city walls so they can warn others about a potential threat. The Lord commands Ezekiel to warn the people if they continue to sin they will surely die, however, failure to warn them would make him personally responsible for their sin. The Lord, therefore, places a grave responsibility on the priest-prophet to admonish sinners.

Of course, the challenge of warning people of sin is the harsh reality that exposing one’s sins will evoke a backlash. The funny thing about sin is how comfortable we get with it. It is something like an acquired taste. For instance, when someone tries an alcoholic drink for the first time, the reaction is often to spit it out. But, after sufficient encouragement and will, one may grow accustomed to drinking. If one were to knowingly drink to excess, they would become drunk, which is a mortal sin. Why is it a mortal sin to knowingly become drunk? Because, according to St. Thomas Aquinas, by becoming drunk, one knowingly deprives oneself of the use of reason. Reason is one of the ways in which we are like unto God. In fact, St. John calls Jesus Christ the logos or reason. To knowingly deprive ourselves of reason is to knowingly deprive ourselves of that which makes us an image of God—it is to deprive ourselves of Christ. And so, we exercise our reason through the virtues of prudence and temprance to avoid the sin of drunkenness.

Just as the Lord made the priest-prophet Ezekiel a watchman over the house of Israel, so too has Jesus made his priests watchmen over the Church. But, what about the people? What role do the people have to counsel or warn others of sin? By nature of our baptism, all the faithful have a share in the threefold mission of Jesus, who is priest, prophet, and king. And since the role of a prophet is to teach, whenever we teach one another, as a father his son, a mother her daughter, a catechist their student, a priest his congregation, we are exercising the prophetic ministry of Christ.

Jesus tells his disciples, “If your brother sins against you, go and tell him his fault between you and him alone.” This is the first step in reconciling with another, but it is also the first step in overcoming sin. If someone does not realize they are sinning, how will they know unless someone shows them? Sin must be exposed before one can repent. Jesus then says, “If he does not listen, take one or two others along with you….” This is the next stage of helping someone overcome sin. We bring reinforcements. If that fails, then we are to tell the Church. In other words, speak to your priest about what is going on. The priest may have some advice. Most importantly, the priest can pray for the person in trouble. Lastly, if that intervention does not work, then we are to distance ourselves from that person. In every case, we make these attempts out of love for our neighbor and care for their immortal soul. God-willing, our admonishment will be fruitful and we will win over our brothers and sisters.

And so, as we continue to reflect on the importance of warning others about sin, may the communion we share—our union with Christ—give us the courage to reconcile with those who have sinned against us and to renew the love we have for one another. As Jesus says, “For where two or three are gathered together in my name, there am I in the midst of them.”

---
