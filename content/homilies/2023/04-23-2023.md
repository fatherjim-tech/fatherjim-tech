+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "easter", "2023"]
date = "2023-04-23"
description = "Third Sunday of Easter, Year A"
searchdesc = "The disciples on the road to Emmaus show us the ideal place to encounter Jesus is in the breaking of the bread. Do we recognize Jesus as the disciples do in the breaking of the bread?"
image = "/img/homilies/jesus-emmaus.jpg"
featured = "jesus-emmaus.jpg"
featuredalt = "Jesus walking with the disciples on the road to Emmaus"
featuredpath = "/img/homilies/"
linktitle = ""
title = "Recognizing Jesus in Our Midst"
type = "post"
format = "homily"

+++

# Readings:
## Acts 2:14, 22-33; Ps. 16; 1 Pt. 1:17-21; Lk. 24:13-35

{{< audio "/audio/2023/homily-third-sunday-easter-year-a-2023.mp3" >}}  
*(Audio recorded live, 23 April 2023)*

Today, we continue to focus on the awesome reality of Jesus’ resurrection. St. Peter gives powerful testimony to what he and the disciples have witnessed with their own eyes and links it to the prophecy of David, which we heard repeated in the Psalm, “You will not abandon my soul to the netherworld, nor will you suffer your faithful one to undergo corruption.” St. Peter’s  letter reinforces the sacrificial nature of Jesus’ crucifixion so that those who accept the testimony of the apostles may continue to hope in God. That hope comes to us in the form of the resurrection, but not without our own form of sacrifice. St. Peter reminds us that we were ransomed from our sins by Christ, so we are to conduct ourselves with reverence during our journey of faith. And what better portrayal of our journey of faith with Jesus than today’s Gospel of the disciples on the road to Emmaus?

One of the great contrasts in our Gospel passage is the unbelief of the disciples on the road to Emmaus and their utter astonishment when they recognize Jesus at table. What they recognized in the breaking of the bread is what they had failed to realize all along. That is why they say, “Were not our hearts burning within us while he spoke to us on the way and opened the Scriptures to us?” Their hearts were burning because they were acknowledging the truth in what Jesus had spoken to them; not only the truth, but the fulfillment of that truth in Jesus’ resurrection. Their hearts were burning while Jesus spoke to them on the way. In other words, Jesus walks with his disciples throughout their journey, even when they do not seem to recognize him. Jesus walks with us, too, most especially when we are distressed.

How does Jesus walk with us? Well, in one sense, Jesus is with us in the Spirit, which knows no bounds; in another sense, Jesus is with us in the liturgy, our works of worship and prayer; in another sense, Jesus is with us in the ministers of the Church, the priests and deacons; in another sense, Jesus is with us in the proclamation of his Word, which, causes our hearts to burn, too. Of course, most importantly for us, Jesus is with us in the Eucharist.

The Eucharist is the sacrament par excellence, which is why the bishops are encouraging a Eucharistic Renewal, especially for the American churches. There indeed has already been a renewal in longing for the Eucharist, no doubt brought on by the lock downs. But, lately I have been speaking with many people who have been away from church and are now making their way back. In a sense, those who have been away are an image of the disciples on the road to Emmaus, who fell away because they lost hope. It was not until they encountered the risen Lord that they realized their hearts were already burning within. It is no different for the former wayward among us.

It is natural to sometimes struggle with our faith; it is also natural for us to have unmet expectations. But, sometimes expectations are the seeds of resentment. How often do we let our unmet expectations be the source of our anger? What if we focused rather on why we have certain expectations and whether or not they are reasonable? Remember what Jesus says about the unprofitable servants and what they are to say upon the Master’s return. Jesus says, “When you have done all you have been commanded, say, ‘We are unprofitable servants; we have done what we were obliged to do.’” (Lk. 17:10). In other words, the work of the disciple is not about what’s in it for me, but rather what’s in it for others.

The disciples on the road to Emmaus show us the ideal place to encounter Jesus is in the breaking of the bread. In a few moments, I will do as Jesus did the night of the Last Supper and take bread, give thanks, break the bread, and give it to disciples, saying, “Take and eat. This is my body.” Now here is one expectation that will never disappoint. Will we recognize Jesus in this breaking of the bread?

---
