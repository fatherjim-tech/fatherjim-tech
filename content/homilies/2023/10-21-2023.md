+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "2023"]
date = "2023-10-21"
description = "Twenty-ninth Sunday in Ordinary Time, Year A"
searchdesc = "One of the ways we encounter Jesus on our journey is in the poor. We have many different opportunities to provide assistance and aid to those less fortunate among us and around the world."
image = "/img/homilies/hearts-on-fire-feet-on-the-move.png"
featured = "hearts-on-fire-feet-on-the-move.png"
featuredalt = "Hearts on Fire, Feet on the Move"
featuredpath = "/img/homilies/"
linktitle = ""
title = "World Mission Sunday - Give to God What Belongs to God"
type = "post"
format = "homily"

+++

# Readings:
## Is. 45:1, 4-6; Ps. 96; 1 Thess. 1:1-5b; Mt. 22:15-21

Today, we observe World Mission Sunday. As we have been hearing in our announcements, Pope Francis has chosen the theme, “Hearts on fire, feet on the move,” which is inspired by the story of the disciples on the road to Emmaus (cf. Lk. 24:13-35). In this passage, the disciples are leaving Jerusalem depressed because Jesus had been crucified, and along the way, Jesus appeared to them and spoke with them about the Scriptures, how it was necessary for the Messiah to suffer death so he could redeem all mankind. They then have supper, and Jesus took bread, gave thanks, broke the bread and gave it to them. In that moment, they realized who he was and he vanished from their sight. Then they said, “Were not our hearts burning [within us] while he spoke to us on the way and opened the scriptures to us?” So often throughout our faith journey, we are on the move, like the disciples on the road to Emmaus, but like those same disciples, we are often unable to recognize Jesus along the way. Yet, just as Jesus walked with his disciples, so too, he walks with us.

One of the ways we encounter Jesus on our journey is in the poor. We have many different ministries here that seek to provide assistance and aid to those less fortunate among us. And I would invite you to visit one of the many tables in the gathering space to learn more about these outreach opportunities and how to get involved. But, what about other areas in the world? How can we support those in underdeveloped countries? For this, the popes have long supported the missions around the world by way of Pontifical Mission Societies.

Last week, we celebrated the feast of St. Isaac Jogues, our patron. St. Isaac was a priest of the Society of Jesus who came to North America in the mid-1600s as a missionary to teach the Gospel to the native peoples. He joined the work of St. Jean de Brébuf, who had created a translation of the Gospel for the Huron peoples. Just as St. Isaac joined St. Jean in carrying out the mission of spreading the Good News of Jesus Christ, so too, are we called to support the missionary work of the Church in other parts of the world, particularly those countries stricken by poverty. Donations collected today, will provide annual subsidies to missionary dioceses, and to support mission seminaries and religious formation houses, the education of children in mission schools, the building of chapels and churches, as well as sustaining homes for orphaned children, the elderly and the sick. Your generous support will help to promote the spread of the Gospel throughout the world, and indeed carry out the great commission of Jesus to go teach all nations.

In our Gospel today, Jesus says, “[R]epay to Caesar what belongs to Caesar and to God what belongs to God.” We all know what it means to pay taxes. If we use government services, then we are obliged to pay for those services. But, Jesus raises the bar when it comes to taxes. He says we are to give to God what belongs to God. In other words, we should also be concerned with the good deeds that belong to God. As Jesus has said, we should store up treasures in heaven. These treasures are acts of charity, kindness, support for one another, especially along the way of our Christian journey. In a word, those acts which focus on what is best for others.

We see a model of this in Paul’s letter to the Thessalonians. He says, “We give thanks to God always for all of you…calling to mind your work of faith and labor of love and endurance in hope of our Lord Jesus Christ….” Here he highlights four main aspects of their response to the Gospel: faith, love, endurance, and hope. In other words, the good works they do are works of faith, and they labor for their love of Jesus and one another, enduring hardships with the firm hope that the Lord Jesus will come again. We are all called to do the same.

And so, as we seek to repay to God what belongs to God—our good deeds and our love of neighbor—let us prayerfully consider how we can be of help and support by volunteering here in our parish community, but also how we may provide assistance to those remote regions around the world. Of course, if we are not in a position to help with our time or financial support, then let us pray that the Church may continue to bring the Good News of Jesus Christ to all. And may our sharing in the Body of Christ help to strengthen our faith and charity, as we place all our hope in Christ.

---
