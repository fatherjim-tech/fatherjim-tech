+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "easter", "2023"]
date = "2023-05-07"
description = "Fifth Sunday of Easter, Year A"
searchdesc = "Jesus says, “I am the gate.” In this sense, he is talking about how he is the way, the truth, and the life; how no one comes to the Father except through him."
image = "/img/homilies/angelico-niccolina-deacons.jpg"
featured = "angelico-niccolina-deacons.jpg"
featuredalt = "Ordination of the seven deacons"
featuredpath = "/img/homilies/"
linktitle = ""
title = "We Are All Living Stones in the Building of the Church"
type = "post"
format = "homily"

+++

# Readings:
## Acts 6:1-7; Ps. 33; 1 Pt. 2:4-9; Jn. 14:1-12

{{< audio "/audio/2023/homily-fifth-sunday-easter-year-a-2023.mp3" >}}  
*(Audio recorded live, 6 May 2023)*

“[W]hoever believes in me will do the works that I do, and will do greater ones than these, because I am going to the Father." Those who believe are called to do great things in the Church and in the sight of the Lord. All this has been made possible through the Paschal Mystery of Christ, namely, his suffering, death, resurrection, and ascension into heaven. In a few moments, we will all recite the Nicene Creed, and affirm our belief in Jesus, who, now seated at the right hand of the Father, continually intercedes for us. Jesus says, “whoever believes in me will do the works that I do,” works of healing, works of teaching, works of sanctifying, works of preaching, works of prophesying, works of righteousness, works of peace, works of love, “and will do greater ones than these.” Certainly the apostles had no idea the work that lay ahead of them, but their belief in Christ gave them the courage to carry the message of the gospel to the ends of the earth.

We, too, are called to be witnesses to Christ. St. Peter reminds us that we are all living stones that make up a great building, the Church, founded on top of the cornerstone, which is Christ. While Christ was rejected by the chief priests and the elders, he nevertheless became the solid foundation upon which the Church was built, a Church of sinners, yet a Church of saints, each with their own place in the great wall of living stones.

Today, we learn especially about those who have a significant role to play in the Church, the deacons. In the Acts of the Apostles, St. Luke recounts for us the way in which the office of deacon was instituted by the Church. It grew out of the need to care for widows who were being neglected. The apostles knew, since they were entrusted with preaching the gospel, they must not neglect their study of Sacred Scripture, so they appointed seven reputable men, filled with the Spirit and wisdom, to carry out this important work of service in the Church. As time went on, their roles were expanded to include service at the altar, preaching, teaching, and various works of charity. Today the diaconate is both a permanent office for those men living out the vocation of marriage, as well as a transitional one, for those men pursuing the priesthood. In both instances, men called to the diaconate serve the Church in all humility and graciousness, and are often called from the communities they serve.

Jesus says, “whoever believes in me will do the works that I do.” How have we been called to serve? How are we doing the works Jesus does? Do we remember what those works are? I had mentioned a few, but what about his miracles? Do we have any modern miracle workers among us? Have you ever asked God to take care of someone and see that someone healed? Have you ever prayed your way through a terrible storm on the road? Have you ever been given a second chance by someone you never thought would? Sure, these may not be raising someone from the dead, but then again, do we have an EMT here or a doctor? We are all doing the works of Jesus one way or another, but do we recognize it when it happens? And when we do recognize it, do we attribute it to our own effort alone, or to the grace that comes to us because Jesus is constantly interceding for each of us? He says, “you will do greater ones than these, because I am going to the Father.”

Of course, the greatest miracle of all is Jesus’ real presence in the Blessed Sacrament. This is possible because we believe and we acknowledge what Jesus has said and done to be the truth. For he is the way, the truth, and the life. No one comes to the Father except through him, therefore, we seek to be united with Christ in the miracle of Holy Communion. So, may the communion we share today help us to recognize the good works we do as living stones in the Church, and may our reception of this most sacred food, unite us with Christ, who inspires the good we do, who hears our prayers, and answers them according to his will.

---
