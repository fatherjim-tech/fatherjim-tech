+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "2023"]
date = "2023-01-15"
description = "Second Sunday in Ordinary Time, Year A"
searchdesc = "We are a light to the nations. If we want our light to shine to the ends of the earth, we should witness to Christ and His Church as John the Baptist does in the Gospel."
image = "/img/homilies/john-the-baptist-pointing-to-jesus.jpg"
featured = "john-the-baptist-pointing-to-jesus.jpg"
featuredalt = "John the Baptist pointing to Jesus"
featuredpath = "/img/homilies/"
linktitle = ""
title = "We Are Called to Witness to the Lord"
type = "post"
format = "homily"

+++

# Readings:
## Is. 49:3, 5-6; Ps. 40; 1 Cor. 1:1-3; Jn. 1:29-34

{{< audio "/audio/2023/homily-second-sunday-ot-year-a-2023.mp3" >}}  
*(Audio recorded live, 15 January 2023)*

The prophet Isaiah speaks about the importance of perseverance in faith. He is addressing the remnant of Israel, the people, who despite being persecuted and exiled from their homeland, kept the faith, nevertheless. It is to these people the Lord makes this promise: “I will make you a light to the nations, that my salvation may reach to the ends of the earth.” What might this look like today? Let us consider those people who are faithful in our life. What is it about these people that sets them apart from someone without faith? Perhaps we might ask them what draws them to this the Church. Why are they here? Each, in their own way, is a light to the nations. Because most of them, by the grace of God, recognize that they are an integral part of this community; that their faith is part of what keeps the Church alive in this place.

Naturally, after we learn from others why they do what they do, we should also ask ourselves: Why am I here? Do we agree with the reasons others give, or do we have our own reasons? Perhaps our reasons are fundamental: we’re fulfilling our obligation to go to Mass; perhaps our reasons are personal: our family goes, so we go; perhaps our reasons are pastoral: we feel called to go and to share our gifts and talents with the community; perhaps our reasons are intellectual: we are seeking the Truth and want to learn what the Church has to say. I am sure there are plenty of other reasons we can discover by spending some time reflecting on it.

Finally, after we have asked ourselves: Why am I here? We should also ask God. Jesus said to his disciples, “As the Father has sent me, so I send you” (Jn. 20:21). Why had God sent us? What has God sent us to do? The better understanding we have of having been sent on a Divine mission by Christ, the more we will reflect that light of Christ within us. Of all the reasons we have to be here, the most significant is that God desires it because He loves us.

So, how do we respond to this divine invitation? Well, we come to Church, yes. But, what can we do that goes beyond merely fulfilling our obligation? Well, consider what John the Baptist does in the Gospel: He gives witness to Christ. The word “witness” shows up throughout the Bible, especially whenever we consider the prophets, who speak for God, but also the martyrs. John the Baptist was both. The definition of the word martyr is: one who bears testimony to faith. I can think of no better way to respond to the faith that God has given each of us than to testify to that faith. We do this by our presence in Church, yes, but also in the disciplined way we live our lives, observing the Commandments, caring for others, sharing our gifts and talents, and the like. Perhaps most importantly in our world today, we testify to the faith whenever we defend it to those seeking to destroy it. If we want our light to shine to the ends of the earth, we should witness to Christ and His Church. This is the message of our Gospel today.

This month, there is an important way we can all give witness to Christ. On Friday, January 20, our Diocese will celebrate its “Standing Together for Life Mass” at 11AM in the Co-Cathedral in Freehold. Since the reversal of Roe v. Wade has placed responsibilities regarding abortion on the states, the Diocese is holding this day here in-state. Then, next Monday will be our parish observance of the Day of Prayer for the Legal Protection of the Unborn. By attending one or both of these Masses, we are praying for the most fundamental right anyone has: to live. As followers of Jesus, we are all called to promote and defend life. Jesus says, “A thief comes only to steal and slaughter and destroy; I came so that they might have life and have it more abundantly” (Jn. 10:10). I would also encourage us to pray the Rosary on these days, offering it for the protection of the unborn. Meditate on the Joyful mysteries and reflect on the way in which the Son of God chose to enter into our world, through the womb of the Virgin. May our celebration of the Eucharist today strengthen our witness to Christ, who is our life, and who remains with us always by giving us his life that we may all have it more abundantly.

---
