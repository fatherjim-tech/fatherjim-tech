+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "2023"]
date = "2023-07-02"
description = "Thirteenth Sunday of Ordinary Time, Year A"
searchdesc = "In the Gospel of Matthew, Jesus is presented as a new Moses, the true interpreter of the Law. He clarifies the ancient Law in his Sermon on the Mount."
image = "/img/homilies/church-of-saint-isaac-jogues.jpg"
featured = "church-of-saint-isaac-jogues.jpg"
featuredalt = "Church of St. Issage Jogues"
featuredpath = "/img/homilies/"
linktitle = ""
title = "I Have Found My Life In Christ"
type = "post"
format = "homily"

+++

# Readings:
## 2 Kgs. 4:8-11, 14-16a; Ps. 89; Rom. 6:3-4, 8-11; Matt. 10:37-42

<!-- {{< audio "/audio/2023/homily-twelfth-sunday-ot-year-a-2023.mp3" >}}  
*(Audio recorded live, 25 June 2023)*-->

### Fr. Jim's First Weekend at the Church of St. Isaac Jogues

It is a joy to be here as your new priest and am looking forward to getting to know you. Every year, Bishop O’Connell petitions the priests of the diocese if they would like to remain where they are or be transferred. After 5 years in my previous assignment of St. Aloysius Church in Jackson, I told the bishop I would one day like to be considered for a pastorate. I honestly did not expect him to respond so quickly to my request, but when he called to inform me that Fr. Pfleger was retiring and that he would like me to be the Administrator of the parish, I was delighted. This is such a beautiful church with such a wonderful history.

Of course, since Fr. Pfleger was the founding pastor, it will be a bit of adjustment for us all. He did so much for this community and I am sure he will be missed. And I stand with this community in gratitude to the many years of service and dedication to priestly ministry that Fr. Pfleger has given to this parish.

As for myself, I hope you will be patient with me as we get to know each other. You will learn that I have a deep love for the liturgy and the sacraments because it is how we encounter God. And I hope to learn how best to minister to your needs both as a parish and one on one. We have all had our share of challenges, I am sure, but what happened in the past is over. Now is a time to focus on God, who is eternally present. This presence heals and guides us, nourishes and sustains us, as we continue our pilgrim journey on earth. It is my sincere hope that I will be the shepherd Christ has called me to be for you.

Now, by way of introduction, I am no stranger to this part of the diocese. Before I went to seminary, I used to frequent the Sam Ash Music store over in Cherry Hill. I got my very first bass guitar from that store. It was a gift from from my parents and girlfriend at the time—a fire engine red Yamaha four-string with dual Humbucker pickups. From there, I went on to form or join several banks playing all types of music from hard rock to pop. I wrote and recorded a bunch of tracks with various groups and played a lot of places, including PJ’s in Maple Shade, the TLA in Philly, and Seacrets in Ocean City, Maryland, to name just a few. While I was playing in the bands, I worked full time managing an independent jewelry store in Lawrenceville. It was a great job, and I enjoyed helping people pick out their engagement rings, wedding bands, and other beautiful jewels for happy occasions.

I came to the priesthood by way of marriage. There are not many priests who can say they have been married before, but that is indeed the case for me. As a musician, I always thought that I would meet the girl of my dreams at a show. I did meet someone, and we did hit it off, but for all the wrong reasons. At the time, I was not practicing my faith and had no idea what engagement was all about. See, getting engaged is not just about a fancy diamond and calling each other fiance; getting engaged is about discerning God’s will for your life. In other words, engaged couples should be praying and asking God: “Is marriage what you want for my life? Are you calling me to a vocation of marriage?”

For me, I just did what most people think to do when they get engaged, they try and figure out where to have the wedding. It dawned on me that I should probably get married in church. So, I met with the pastor of my home parish and he helped me more fully embrace my parish. When I told him I was considering other places, he said, “No, Jim, this is your parish. You get married in your parish.” From that moment the Spirit started leading me. Then the idea popped into my head that if I am going to get married in church the least I could do is show up on Sunday. Funny how that Catholic guilt works, but it was the right thing to do. So I went to confession, then began to attend Mass. Before I knew it I was going to Mass every day, joining the Bible Study, prayer groups, praying the Rosary every day, going on retreats, visiting Adoration Chapels at different churches. And then, it happened. Someone came up to me in church and asked if I was studying to be a priest. I thought, me? A priest? I’m supposed to get married. How could someone even think that? Before I knew it, more and more people would ask me the same question. My mom would tell me about parishioners who thought I would make a good priest. It was as if it came out of nowhere, but looking back, I know it was coming directly from God. The people could see it, but I was blinded by wedding favors, food tastings, buying a house, and a serious health condition, all of which made for a very difficult engagement. Then, we did what every couple who knows their relationship is broken, but thinks getting married will fix: We got married. Well, some counseling sessions, and a few months later, we parted ways and I set my sights on proper discernment of this call to Holy Orders.

The priesthood is not for the faint of heart. God tries us as gold in the furnace so that our faith may be made pure. Once I had completed the necessary paperwork with the diocese for an annulment, I began to attend community college to get enough credits to qualify for undergraduate studies at St. Mary’s Seminary in Baltimore. After three long years of working, gigging, and night school, I entered the seminary. I was ordained in 2018 by Bishop O’Connell and have been serving in the diocese ever since. And it is my honor to have been assigned to St. Isaac’s to be your priest.

There is plenty more to the story, but I hope that gives you some idea of the man whom the bishop has assigned to shepherd this parish. I may not have all the answers, but I can relate to a lot. And hopefully, we will all grow in faith together. Pope Francis, at the beginning of his pontificate encouraged priests to “be shepherds with the smell of the sheep.” In other words, be close to the flock, walk with them, encourage them, carry them, guard them, correct them. In a word, love them. I hope that I can do this for you.

Jesus says, “Whoever loves father or mother more than me is not worthy of me, and whoever loves son or daughter more than me is not worthy of me; and whoever does not take up his cross and follow after me is not worthy of me. Whoever finds his life will lose it, and whoever loses his life for my sake will find it.” I can honestly say I have found my life, and it is a life in Christ. So, may the communion we share today, be a sign for us all, who strive to lead the life God called us to live.

---
