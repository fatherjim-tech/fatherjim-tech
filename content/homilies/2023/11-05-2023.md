+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "2023"]
date = "2023-11-05"
description = "Thirty-first Sunday in Ordinary Time, Year A"
searchdesc = "Today, the Word of God speaks to us about the value of integrity: Integrity in who we are as people, integrity in what we say, and integrity in how we act."
image = "/img/homilies/pharisees.jpg"
featured = "pharisees.jpg"
featuredalt = "Pharisees with large phylacteries"
featuredpath = "/img/homilies/"
linktitle = ""
title = "We Are Called to Be People of Integrity"
type = "post"
format = "homily"

+++

# Readings:
## Mal. 1:14b-2:2b, 8-10; Ps. 131; 1 Thess. 2:7b-9, 13; Mt. 23:1-12

Today, the Word of God speaks to us about the value of integrity: Integrity in who we are as people, integrity in what we say, and integrity in how we act. The prophet Malachi speaks of the lack of integrity the people had with respect to God. They were conspiring against the Lord and failing to honor the covenant. Jesus also identifies how the Pharisees are acting as hypocrites. They were seeking seats of honor, widening their phylacteries, and lengthening their tassels. In other words, all their actions were for show. Jesus says they do not practice what they preach. It is no different for us today. There are many who do not practice what they preach. So often it is “rules for thee, not for me,” and it is no different for us today. But, Jesus wants his disciples to look beyond appearances, to look beyond their empty show. Jesus wants each of us to look inward and to overcome our own temptation to hypocrisy, our own temptation to say one thing and do another.

At the heart of hypocrisy is a temptation to put on a mask. To put forward a different image of ourselves. It is hard work to look inward and assess oneself. We have to overcome our own worst fears, namely, that we are capable of far worse than we want to admit. That is the power of human freedom—that we can do so much for good, while at the same time be so broken on the inside. Jesus knows well our brokenness, which is why he sought to be broken for us on the cross. And so, he teaches his disciples, and indeed all of us, how to avoid falling into these traps. The teaching is simple: humility.

Jesus says, “The greatest among you will be your servant. Whoever exalts himself will be humbled; but whoever humbles himself will be exalted.” Within the context of the passage, the Pharisees were seen, in many people’s eyes, to be the greatest among them. They represented the most devout of the people, the ones who studied and taught the Law, whose outward show conveyed the idea that they were “holier than thou.” This “holier than thou” mentality is precisely what Jesus is combating. And the juxtaposition is quite stark: Jesus, the Son of God, was born in a lowly stable, whose foster father was a lowly carpenter, and who himself went about from town to town carrying no possessions. For all intents and purposes, Jesus was as humbled as one could be, yet he is going face-to-face with the Pharisees who are as high and mighty as they could be—at least in the eyes of the people. But, Jesus shows how corrupt they truly are by exposing their empty outward show.

St. Paul knew a thing or two about outward show. He was himself a Pharisee who persecuted the Church. But, after his encounter with the Lord Jesus, he became one of the greatest missionaries in the Church. He founded several Christian communities throughout his travels, including the church in Thessalonica. This was the first community he established, which is also why he writes to them in such an endearing way. Listen to what he says to that community: “Brothers and sisters: We were gentle among you, as a nursing mother cares for her children. With such affection for you, we were determined to share with you not only the gospel of God, but our very selves as well, so dearly beloved had you become to us.” This is not just a letter from a missionary; this is a letter from the heart of one who loves his people like a father loves his family. Then he goes on to say the most important part of the passage: “And for this reason we too give thanks to God unceasingly, that, in receiving the word of God from hearing us, you received not a human word but, as it truly is, the word of God, which is now at work in you who believe.” St. Paul is praising the fact that the people did not embrace Christ because of some outward show, but rather because they heard the word of God and welcomed him into their hearts. This can only happen through humility. First, through the devoted preaching of St. Paul, and second by the way he lived his life, becoming one with the community—not placing any burdens on them, save for the responsibility that comes from living the Gospel. In a word, St. Paul practiced what he preached, and the people loved him for it.

We are called to do the same: To live our lives with integrity, to practice what we preach, to put the Gospel into action daily, to pick up our crosses daily, as Jesus commands. This is the life of the Christian. It is not always an easy life, but what we are called to is far easier to do than what the world pushes in our faces. We are constantly bombarded with negativity, hatred, anger, and deceit. It is everywhere, on all the news channels, throughout social media, and sadly even in our own homes. What we lack is humility. We have to accept that we are not God, and let God be God in our lives. This can only happen through humility. And so, as we seek to humble ourselves before one another and Almighty God, let us be mindful of our need for God’s love and mercy in our lives. And as we approach the Altar of Sacrifice, let us humble ourselves before our God, who humbled himself to share in our humanity. May the communion we share unite us in the love of Christ, who is our true King, the one who makes all things new and truly makes us free.

---
