+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "2023"]
date = "2023-09-03"
description = "Twenty-second Sunday in Ordinary Time, Year A"
searchdesc = "Jesus says, 'Whoever wishes to come after me must deny himself, take up his cross, and follow me.' To deny oneself is to disown oneself as the center of one’s existence. In other words, I am not God, no matter how much authority I am given."
image = "/img/homilies/jesus-peter-get-behind-me.jpg"
featured = "jesus-peter-get-behind-me.jpg"
featuredalt = "Jesus telling St. Peter to get behind him"
featuredpath = "/img/homilies/"
linktitle = ""
title = "Are We An Obstacle to Jesus?"
type = "post"
format = "homily"

+++

# Readings:
## Jer. 20:7-9; Ps. 63; Rom. 12:1-2; Mt. 16:21-27

Last week, I spoke of the authority Jesus gives to St. Peter to bind and loose sins. This week, Jesus tells his disciples he will be killed in Jerusalem and Peter takes Jesus aside to rebuke him. Immediately after Jesus entrusts Peter with the keys to heaven and earth, Peter does what many in authority are tempted to do, they try and wield that authority over others, even the Son of God. Jesus says to Peter, “Get behind me, Satan! You are an obstacle to me." Jesus shows the true nature of such temptations by calling Peter, “Satan.” Earlier in the gospel, Jesus casts out a demon by saying, “Get away, Satan!” In today’s passage, Jesus says essentially the same thing, but adds, “You are an obstacle to me.” This line is unique to Matthew’s gospel; we do not see it in Mark’s account. It would seem as though Matthew wants to expand on what it means to be called “Satan.” The Greek word is *Satana*, but it a transliteration of the Hebrew word, *haś·śā·ṭān*, which means an opponent. In other words, Jesus is warning Peter that he has allowed himself to be overcome by a spirit of opposition. There are times when it is good for us to have a spirit of opposition, for instance, when opposing injustice or defending the innocent. It is good to oppose the aggressor in such situations. But, by opposing Jesus to his face, Peter has clearly crossed the line.

If Jesus says he is going to be killed in Jerusalem, why might it be wrong for Peter to oppose him? Jesus explains this when he says, “You are thinking not as God does, but as human beings do.” Peter was thinking about preserving Jesus’ earthly life, holding on to him for his own selfish reasons rather than conform his will to that of the Father. Jesus had already resolved to go to Jerusalem. He knew what he had to do to fulfill all righteousness. He knew this was what God sent him to accomplish. That is why, in the garden of Gethsemane, Jesus says to his heavenly Father, “Not my will be done, but your will.” But, there was Peter, the new pope, thinking he could tell the Son of God what was best for him. Jesus surely cut him to the quick and showed him that the mission he has been given is non-negotiable because it is from Almighty God.

Many of us can relate to this gospel passage. Many of us have been in positions of authority, have been entrusted with certain responsibilities, whether at home or in the workplace. We all have a duty to care for one another. But, if we allow ourselves to be tempted to use our authority in such a way as to enrich ourselves, we have already sold our soul. Jesus asks, “What profit would there be for one to gain the whole world and forfeit his life?” Do we use our power and authority for the good of all, or do we use it for personal gain? God’s will was for his only begotten Son to offer himself as a sacrifice to redeem us from sin and death. This is how Jesus wielded his power, for the the good of all. Peter, on the other hand, sought to hold on to Jesus for himself, and for doing so, he made himself an obstacle to Jesus.

How often do we oppose Jesus in our life? Do we really grapple with our faith? Are there certain teachings in the Church that we struggle to accept? Jesus is the perfect example of how we should approach our faith: he went all-in for his heavenly Father. Can we do the same? Can we give ourselves completely to Jesus as he gave himself completely for us?

St. Paul encourages us not to be conformed to this age, but be transformed by the renewal of our mind. This is a time of renewal in the Church. Many are realizing that they need God in their life. How are we there for these people? Can we be a bridge between them and the Church?

Peter had a hard lesson to learn in today’s Gospel. Jesus showed him how he went wrong and what he must do to follow him. He says, “Whoever wishes to come after me must deny himself, take up his cross, and follow me.” To deny oneself is to disown oneself as the center of one’s existence. In other words, I am not God, no matter how much authority I am given. As Jesus said to Pilot, “You would have no power over me if it had not been given to you from above” (Jn. 19:11). We, too, have a share in the freedom of God, but it is only through letting go that we may be truly free. Otherwise, we are ensnared by our passions, desires, greed, and lust for power. Jesus says, “For whoever wishes to save his life will lose it, but whoever loses his life for my sake will find it.” Our life is here. Our life is with Christ. And the communion we share today is the sign of his presence with us.

---
