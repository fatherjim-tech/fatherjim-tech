+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "advent", "2022"]
date = "2022-12-08"
description = "Solemnity of the Immaculate Conception of the Blessed Virgin Mary, Year A"
searchdesc = "Mary had long since been venerated as the Theotokos, or Mother of God, and Mariology developed alongside the theology of Christ. It is through Mary that Jesus took human form, and in order for Jesus to be born without sin, his mother would also have to be without sin."
image = "/img/homilies/immaculate-conception.jpg"
featured = "immaculate-conception.jpg"
featuredalt = "Immaculate Conception of the Blessed Virgin Mary"
featuredpath = "/img/homilies/"
linktitle = ""
title = "Mary Models For Us How To Be a Reservoir of Grace"
type = "post"
format = "homily"

+++

# Readings:
## Gn. 3:9-15, 20; Ps. 98; Eph. 1:3-6, 11-12; Lk. 1:26-38

{{< audio "/audio/2023/homily-immaculate-conception-year-a-2022.mp3" >}}  
*(Audio recorded live, 8 December 2022)*

Today we celebrate the Solemnity of the Immaculate Conception of the Blessed Virgin Mary. Pope Pius IX instituted this Solemnity in honor of the Blessed Mother. Mary had long since been venerated as the Theotokos, or Mother of God, and Mariology developed alongside the theology of Christ. It is through Mary that Jesus took human form, and in order for Jesus to be born without sin, his mother would also have to be without sin. This doctrine of the Immaculate Conception developed throughout the centuries until Pio Nono declared it formally in 1854.

God chose to make his entry into this world through a special womb. And as we heard in the Gospel, this was no ordinary woman. The angel Gabriel addresses her as “full of grace,” which gives her pause. Angels are of a higher order than man, should not Mary be the one who greets the angel with such veneration? What does it mean to be full of grace? It is certainly one thing to be a channel of grace, as many of us have experienced throughout our Christian lives. It is another thing to be a reservoir of that grace, let alone to be full of grace as Mary was. Indeed, Mary was the only woman conceived without sin, which made her the perfect reservoir of God’s grace so that she could bear the Savior of the world.

While Adam and Eve represent the primordial failure to keep God’s command, Mary’s ‘yes’ to God shows a great reversal. While all of mankind has been tainted by the sin of our first parents, Mary introduces a new paradigm by her fiat, saying, “May it be done to me according to your word.” By saying ‘yes’ to God, she closes the door on sin.

What about us? While we may not be full of grace, can we say we are at least good reservoirs of God’s grace? When we receive a particular grace, are we aware and able to acknowledge it? The more aware we are of the graces we receive, the more graces God will give. We, like Mary, are to be good instruments of God’s grace, to allow that grace to pervade our souls, to transform us, to sanctify us. Mary had no need of transformation because she was born without sin, but we do, therefore, Mary is a model for us all: A model of discipleship, a model for mothers, a model for women. She shows us how to say ‘yes’ to God and to devote herself completely to his holy will.

So, as we celebrate this great feast of Mary’s Immaculate Conception, her freedom from sin, let us remember our own freedom from sin through the blood of Christ. He paid the ultimate cost for us to be free, not so we can do whatever we want when we want, but so we can enjoy true freedom as adopted sons and daughters of the Father. As St. Paul says, “In love he destined us for adoption to himself through Jesus Christ, in accord with the favor of his will, for the praise of the glory of his grace that he granted us in the beloved.” Mary was filled with this grace, Jesus graced the world by his sacrifice, and now that grace comes to us through the sacraments. May we unite our hearts with his as we honor the Blessed Mother today, and may her ‘yes’ to the Word of God be our example, not just during Advent, but for years to come.

---
