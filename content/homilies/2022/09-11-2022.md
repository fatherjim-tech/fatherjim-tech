+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "2022"]
date = "2022-09-11"
description = "Twenty-fourth Sunday in Ordinary Time, Year C"
searchdesc = "Moses appeals to God on behalf of the Israelites and God relents of the punishment he threatened to carry out; St. Paul admits he was a persecutor of the Church, but he experiences the mercy of God by encountering the Risen Lord; Jesus seeks to show how the mercy of the Father extends especially to sinners."
image = "/img/homilies/prodigal-son-returns.jpg"
featured = "prodigal-son-returns.jpg"
featuredalt = "The prodigal son returns to his father"
featuredpath = "/img/homilies/"
linktitle = ""
title = "God Has Mercy on His People Throughout the Ages"
type = "post"
format = "homily"

+++

# Readings:
## Ex. 32:7-11, 13-14; Ps. 51; 1 Tim. 1:12-17; Lk. 15:1-32

{{< audio "/audio/2022/homily-twenty-fourth-sunday-OT-year-c-2022.mp3" >}}  
*(Audio recorded live, 11 September 2022)*

Today, we focus on the mercy and unconditional love of God. It is sometimes difficult for us to square the image of God as portrayed in the Hebrew Scriptures with the image of God as portrayed by Jesus. Sometimes, the God of the Old Testament appears to be an angry God, without compassion, and quick to punish. Some Gnostics even promoted that the God of the Old Testament was a different God than the God of Jesus. The Gnostics were sects who promoted a hidden or secret knowledge. But, there is nothing secret with God. As we heard the father say in the gospel: “Everything I have is yours.” Does this sound like a God who keeps secrets? On the contrary, our God is one who announces to the world who he is, what he is doing, what he expects from his people, and like the father in the gospel, runs to his lost child and embraces him. So, let us look at the merciful image of God that is being described to us today.

First, in the Book of Exodus, the Israelites turned to the worship of a golden calf, which angered God. He is a jealous God and commands that the people shall have no other gods before him. Meanwhile, the people had been sacrificing to a false idol, saying, “This is your God, O Israel, who brought you out of the land of Egypt!” Now, we might ask ourselves: Would not God be justified in wiping that wicked people out? They ascribed to a statue of an animal, a grass eating bull as the psalms say, their freedom from Egypt. Does this sound like the kind of people that deserves God’s mercy and forgiveness? Now, one might say, they were ignorant. They were merely resorting to what they already knew by worshiping the golden calf. After all, that was the religion of the Egyptians. But, God had delivered the Israelites out of Egypt, out of their system of slavery, out of their bondage. Yet, they resort to the same old ways. So, God threatens to consume them and start over once again, as he did with Abraham. Moses, however, appeals on behalf of the people. He recounts the promise made to Abraham, that his descendants would be as numerous as the stars in the sky; he recounts the covenant. And since the Israelites were the rightful heirs to the covenant, going all the way back to Abraham, God relents of the punishment he threatened to inflict on the people. God shows them mercy. They may be ignorant, but they are still the children of Abraham. The rest of their time in the desert will be spent growing in the knowledge and fear of the Lord.

St. Paul describes a similar circumstance in his first letter to Timothy. He recounts how he was once a blasphemer and persecutor of the Church, but was mercifully treated because he acted out of ignorance. Once again, there is a problem with ignorance. He did not realize who he was persecuting. As it says in the Acts of the Apostles, “a light from the sky suddenly flashed around him. He fell to the ground and heard a voice saying to him, ‘Saul, Saul, why are you persecuting me?’ He said, ‘Who are you, sir?’ The reply came, ‘I am Jesus, whom you are persecuting’” (Acts 9:3-5). Paul needed to encounter the Risen Lord before he could amend his ways, and after he was baptized became the greatest missionary of the Church.

These are two brilliant examples of how God, in his mercy, allows people to discover who he truly is. What about our Gospel passage? We often look at this passage with a little resentment in our hearts. We often feel the older brother was justified in the way he was acting, but that’s not the point of the parable. To appreciate what Jesus is doing with this teaching, we need to look to whom he is speaking.

The passage begins: “Tax collectors and sinners were all drawing near to listen to Jesus, but the Pharisees and scribes began to complain, saying, ‘This man welcomes sinners and eats with them.’ So to them he addressed this parable.” Now, if we were to draw a parallel between the tax collectors and sinners, and the Pharisees and scribes, which groups might the characters in the parable represent? The younger son represents the sinners, and the older brother represent the Pharisees and scribes. Sometimes we ought not to focus on how the passage makes us feel so much as the substance of Jesus’ teaching. Luke is one of the most beautifully written gospels, not just because the words cut us to the heart, but because we receive the essence of Jesus’ teaching.

Pope Francis admits on a regular basis that he is a sinner. He models for us the humility of the life of the Christian. We are all sinners, and therefore, we are all in need of God’s mercy. Who would you rather identify with? The smug, uptight older brother, who has no love for his brother because he squandered his father’s inheritance? Or the broken, humbled, and pleading younger brother, who realizes a life of sin cannot satisfy and returns to his father? Would that we all have this realization on a daily basis that we might see our neighbor as they truly are, a child of God. All this because the image of God that Jesus shows is one of mercy and unconditional love. Remember what the father says, “Everything I have is yours.”

And so, as we seek to experience the unconditional love of God opened to us through the sacrifice of Christ, may the communion we share unite us more intimately with the Father, who loves us so much, and one another, who are his children, “‘a chosen race, a royal priesthood, a holy nation, a people of his own, so that [we] may announce the praises’ of him who called [us] out of darkness into his wonderful light” (1 Pt. 2:9).

---
