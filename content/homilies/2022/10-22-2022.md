+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "2022"]
date = "2022-10-22"
description = "Thirtieth Sunday in Ordinary Time, Year C"
searchdesc = "Jesus highlights improper attitudes in prayer. In today’s Gospel, he speaks about the Pharisee and the tax collector. One is self-righteous, the other humbles himself before God. One of them leaves justified."
image = "/img/homilies/pharisee-and-tax-collector.jpg"
featured = "pharisee-and-tax-collector.jpg"
featuredalt = "Pharisee and tax collector in Temple praying"
featuredpath = "/img/homilies/"
linktitle = ""
title = "How We Pray Matters - A Lesson on Prayer"
type = "post"
format = "homily"

+++

# Readings:
## Sir. 35:12-14, 16-18; Ps. 34; 2 Tm. 4:6-8, 16-18; Lk. 18:9-14

{{< audio "/audio/2022/homily-thirtieth-sunday-OT-year-c-2022.mp3" >}}  
*(Audio recorded live, 22 October 2022)*

Last week, our readings focused us on the importance of persevering in prayer. So, how did we do? Have we persisted in prayer throughout the week, or did other things get in the way? The passage from the Book of Sirach reminds us that God hears our prayers, especially the prayers of the lowly. The first beatitude is: *Blessed are the poor in spirit, for theirs is the kingdom of heaven.* To be poor in spirit is to be humble before God. It goes without saying that none of us is superior to Almighty God, so it should be second nature for us to approach God with humility. How often do we pray to God with this in mind? We know that God is all-knowing, all-powerful, and all-loving, but are our prayers more demands than petitions? Are we attempting to boss God around? Or perhaps we hold God in contempt for not answering our prayers the way we want. These are examples of improper attitudes to have in prayer.
	
Jesus also highlights improper attitudes in prayer. In today’s Gospel, he speaks about the Pharisee and the tax collector. The Pharisee, who is self-righteous, prays to himself, “O God, I thank you that I am not like the rest of humanity -- greedy, dishonest, adulterous -- or even like this tax collector. I fast twice a week, and I pay tithes on my whole income.” Meanwhile, the tax collector stood off at a distance striking his breast asking for God’s mercy. Jesus says the tax collector went home justified. Why? Might it be because the Pharisee was so full of himself that he failed to humble himself before God? Jesus says the Pharisee took up his position, in other words, he had a seat of honor in the Temple, and began listing off all the wonderful deeds he had done, deeds that were righteous in the eyes of everyone else. He was more concerned with himself than he was with God. Perhaps that is why Jesus even says the Pharisee prayed to himself. Have we made this mistake? Have we made ourselves out to be so significant that we forgot that God was above us?

These are the kinds of teachings that make us think. Jesus wants us to pray like the tax collector because it is a true expression of humility before God. No one is perfect, and no one is free from sin, therefore, our desire should be for God’s mercy. When we humble ourselves in this way, our accomplishments are sometimes revealed as obstacles to holiness. In reality, we should do all the things the Pharisee does with respect to the Law, but in our heart, always seek God’s mercy and love. By doing so, when we finish the race, our acts of humility will lead to our being exalted in heaven.

And so, as we continue to persevere in prayer, with all humility, seeking the love of God, may the communion we share today, help us to persevere in prayer, and may the Lord, who reconciles us to the Father, pour out an abundance of his grace upon us.

---
