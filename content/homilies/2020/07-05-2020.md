+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "2020"]
date = "2020-07-05"
description = "Fourteenth Sunday of Ordinary Time, Year A"
searchdesc = "Jesus does not leave us empty-handed, rather, he carries our burdens with us. His burden is indeed light, for it is a simple burden of compassionate love."
image = "/img/homilies/carrying-crosses.jpg"
featured = "carrying-crosses.jpg"
featuredalt = "Jesus carrying his cross followed by thousands of disciples carrying their crosses."
featuredpath = "/img/homilies/"
linktitle = ""
title = "Jesus Calls Us to Love, a Burden that is Light"
type = "post"
format = "homily"

+++

# Readings:
## Zec. 9:9-10; Ps. 145; Rm. 8:9, 11-13; Matt. 11:25-30  

Happy Independence weekend. I hope everyone has been able to take a break from all the bad news and spend time with family, enjoy hot dogs and burgers on the grill, and take it easy. Given our nation is going through a difficult time, I want to encourage everyone to look up the website [archives.gov](https://www.archives.gov/founding-docs/declaration-transcript) and read the Declaration of Independence. I am sure that you will agree that we have much to be thankful for, but also much to safeguard, and visiting our nation’s founding documents will help us keep our American spirit.

Today, I want to attempt to weave a little golden thread through our readings. The prophet Zechariah prophecys that the Lord will come to Jerusalem riding on an ass. This is a foreshadowing of Jesus’ triumphant entrance into Jerusalem as he himself was riding on a donkey. The people proclaimed, “Hosanna in the Highest,” and, “blessed is he who comes in the name of the Lord.” This was a joyful moment, which is captured by our psalm: “I will praise your name forever, my king and my God.”

St. Paul encourages the church in Rome to live in the Spirit of Christ. Jesus teaches his disciples by saying, “learn from me, for I am meek and humble of heart.” The Spirit of Jesus is poor and humble, in other words, he gives of himself to others. He does not exalt himself; he does not look for admiration; he does not seek approval; he does not try to impress; rather, Jesus invites us to follow his example. And he does not leave us to fend for ourselves, but carries our burdens with us. When we live in the Spirit, we are never alone, for Christ is with us always. So, let us be ever mindful of his presence, most especially as we receive him in the Most Blessed Sacrament.

---
*Given during the COVID-19 pandemic.*

*At the request of the bishop and to uphold the common good, all Masses and homilies are to be kept brief to reduce the time of possible exposure to the virus.*

To read the Declaration of Independence online, visit [https://www.archives.gov/founding-docs/declaration-transcript](https://www.archives.gov/founding-docs/declaration-transcript).