+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "2020"]
date = "2020-06-27"
description = "Thirteenth Sunday of Ordinary Time, Year A"
searchdesc = "A disciple of Jesus is not one who runs away from the challenges of the day; the disciple is one who takes to heart the Word of God and seeks to embody those words wherever they go."
image = "/img/homilies/jesus-sending-his-disciples.jpg"
featured = "jesus-sending-his-disciples.jpg"
featuredalt = "Jesus sending his disciples on mission"
featuredpath = "/img/homilies/"
linktitle = ""
title = "We Have Been Comissioned by Christ"
type = "post"
format = "homily"

+++

{{< youtube FEXWz5naZvk >}}

---

# Readings:
## 2 Kgs. 4:8-11; 14-16a; Ps. 89; Rm. 6:3-4, 8-11; Matt. 10:37-42

In last week’s Gospel, Jesus told his disciples not to fear, that they are precious in the eyes of the Lord, that even the hairs of their head are counted. Today, he says, “Whoever receives you receives me, and whoever receives me receives the one who sent me.” Jesus gives these words of encouragement within the context of his commissioning of the Twelve disciples. He sends them forth with the instructions to proclaim the kingdom of heaven, to cure the sick, to raise the dead, to cleanse lepers, and to drive out demons. But, immediately following these instructions, Jesus warns his disciples of the coming persecutions for those who carry out his mission on earth. Such is the difficult fate of those who speak the truth in the world. They are mocked and derided for shining the light in a world shrouded darkness. But, just because someone refuses to accept the truth does not mean there is no truth. Just because it is cloudy out, does not mean there is no sun. The disciple is not one who runs away from the mission, rather, the disciple is one who takes to heart the Word of God and seeks to embody those words wherever they go. Jesus tells us such a discple is precious in the eyes of God and enjoys the same esteem as that of a prophet or a righteous person. Why? Because they speak the truth; they speak the words of God; they represent God. “Whoever receives you receives me, and whoever receives me receives the one who sent me.”

There is a terrible movement in our nation seeking to destroy our way of life. This movement is no longer about justice for one man, it is about destruction—the destruction of who we are as a people, who we are as a culture, who we are as Americans, who we are as disciples of Jesus. There is a particular kind of sin we have all experienced, especially within the last few months; it is the sin of sloth. In my study of this sin, I referenced St. Thomas Aquinas for clarity, and St. Thomas actually references one of the Church Fathers, St. John of Damascus.

{{< img-post-width "/img/homilies/" "st-john-damascene.jpg" "text" "right" "40%">}}

St. John Damascus was a key figure in the fight against the eighth century Iconclast heresy, a movement against the veneration of images. It was Damascus who argued that since Jesus Christ, the Son of God, became man, his likeness, and by extension, the likenesses of Mary and the saints, could be depicted in art. At the time, he would have been focusing on religious icons, but we can apply the same logic to statues, frescoes, stained glass, and the like. Damascus, who is a Doctor of the Church, and sometimes regarded as the first scholastic, had this to say about the sin of sloth: *Sloth is an oppressive sorrow, which, to wit, so weighs upon man’s mind, that he wants to do nothing.* Have we felt like this at all? Have we been so exhausted from all the bad news that we feel paralyzed? If so, you are not alone, but I believe that what we see happening in our nation today is our call to action. Today’s Gospel is not just the commissioning of the Twelve; it is also our commission, our sending forth. St. Paul says, “If, then, we have died with Christ, we believe that we shall also live with him.” He was not talking about our being dead to the world as being complacent, but rather, that we must actively fight against the world with the strength that comes from our life in Christ. We must think of ourselves as dead to sin, dead to sloth, and living for God. This is what it means to be a disciple of Jesus, to die to sin, to embody Christ, and to live for God. Now is the time for us to act, to study the issues, to pray without ceasing, to live for God by giving witness to Christ, in a word, now is the time to live our baptism.

So, as we turn to the celebration of the Eucharist, to receive from this holy altar the Most Precious Body and Blood of Christ, may our communion strengthen our faith so that we may give witness to Christ in a world broken by sin and in desperate need of the light of Christ. Let us be that light to everyone we encounter; let us be sharers of the Good News, defenders of truth.

---
*Given during the COVID-19 pandemic.*

*At the request of the bishop and to uphold the common good, all Masses and homilies are to be kept brief to reduce the time of possible exposure to the virus.*

To read more from St. Thomas Aquinas on the Sin of Sloth, visit [https://www.newadvent.org/summa/3035.htm](https://www.newadvent.org/summa/3035.htm).