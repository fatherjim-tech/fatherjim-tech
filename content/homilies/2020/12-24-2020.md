+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["christmas", "solemnities", "2020"]
date = "2020-12-24"
description = "The Nativity of the Lord, Solemnity (Vigil)"
searchdesc = "This Christmas, we take courage in knowing that Christ has come to be our light, to cast out our fears, to restore us as sons and daughters of the Father, to be our hope, our peace, and our life."
image = "/img/homilies/nativity-scene2b.jpg"
featured = "nativity-scene2b.jpg"
featuredalt = "Image of Star of Bethlehem over a stable with the Holy Family"
featuredpath = "/img/homilies/"
linktitle = ""
title = "This Day our Savior is Born, Alleluia!"
type = "post"
format = "homily"

+++

# Readings:
## Is. 62:1-5; Ps. 89; Acts 13:16-17, 22-25; Matt. 1:18-25

{{< audio "/audio/homily-christmas-vigil-year-b.mp3" >}}  
*(Audio recorded live, 24 December 2020)*

Just this past Monday, there was a significant celestial event, as the planets Jupiter and Saturn came within 0.1 degrees, appearing as a single bright light in the sky, what many have called the “Christmas Star.” The last time this could be observed was almost 800 years ago. Astrologers and New Age gurus are calling this celestial event the dawn of a new age of Aquarius, but as Christians, we recall another celestial event that happened more than two-thousand years ago, we remember the Star of Bethlehem. In those days, it was believed that a new star appeared at the time of a ruler’s birth, and that ruler was Jesus Christ, the Son of God. This Christmas, we recall the awesome mystery of the Incarnation, the birth of Christ, and what it means for we, the baptized.

St. Paul explains how God the set the stage for the arrival of Jesus. He shows how God would raise up a savior born of the house of David. By so doing, he would free his people from the bondage of sin, and usher in a new age of reconciliation, grace, and peace. God does this for no other reason than unconditional love for his people. As the prophet Isaiah says, “For the Lord delights in you and makes your land his spouse.” Those people who were walking in the darkness of sin, oppression, sadness, and fear, were about to have a great awakening, to see a great light, and to be released from captivity. The Lord accomplishes all this through Jesus, who is the Messiah, the newborn King of Israel. The Star of Bethlehem, therefore, becomes a sign to all who await the fulfillment of the Lord’s promise.

Now, nearly 2 millennia later, we still remember this important celestial event. As a light in the heavens, it illumined the path to Jesus, and for we who believe, it dispels the darkness of fear and doubt. Much of the last year has been filled with fear and doubt. The restrictions due to the pandemic are constantly before us; the news channels constantly sew doom and gloom; politicians talk about a dark winter, yet, in spite of it all, here we are. Why is that? When it seems as though the whole world is hell bent on trying to keep us from living our faith, why is it we are still here? Could it be the light of Christ in each of us has strengthened our faith? Could it be that the light of Christ in each of us has strengthened our hope? Could it be that the light of Christ in each of us gives us the courage to overcome fear and doubt?

I don’t know about you, but I remember waking up on Monday just being ornery. It was like one of those days when you wake up on the wrong side of the bed, which is really not like me. Anyway, as I was driving around running errands, I was listening to my favorite playlist and thinking about Christmas. Then I started replaying the events of the last year in my mind: Our struggles with the pandemic, locking down, people loosing their loved ones, loosing their businesses, loosing their jobs. At first, they were saying ‘15 days to flatten the curve,’ next they will say ‘100 days to have a chance.’ And as I reflected on all these struggles, I felt myself being tempted to despair. So, I prayed to God, saying, “Lord, why does it have to be this way? How long do we have to endure this?” And for anyone who has ever wondered if God is listening, well, listen to this. As I’m driving down the road saying prayers of lament, without skipping a beat, the next song started playing. It was Matt Maher’s “Your Grace is Enough.” Listen to the words from the first verse: “Great is Your faithfulness, O God / You wrestle with the sinner's restless heart / You lead us by still waters and to mercy / And nothing can keep us apart…Your grace is enough.” I said, “Okay, Lord. Message received.” I am sure many will recall how St. Paul prayed that the Lord would deliver him from a certain thorn in his side, but the Lord simply says, “My grace is sufficient for you, for power is made perfect in weakness” (2 Cor. 12:9a).

Maybe some of you have felt the same way over the course of this year; maybe some of you have been saying prayers of lament; maybe some of you have been looking for a sign. And in the midst of all the confusion, we have all been led here, to this Church, to this altar, to the One God who has called each of us out of darkness and into his own marvelous light. And so, as we gather to celebrate the Birth of our Blessed Lord, may this Holy Sacrifice of the Mass be a perpetual reminder of His presence among us. And may we, who persevere through otherwise dark days, take courage in knowing that Christ has come to be our light, to cast out our fears, to restore us as sons and daughters of the Father, to be our hope, our peace, and our life. May you and your families have a very healthy, a very merry Christmas.

---
*Given during the COVID-19 pandemic.*