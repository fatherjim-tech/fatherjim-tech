+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "solemnities", "2021"]
date = "2021-06-06"
description = "Solemnity of the Most Holy Body & Blood of Christ, Year B"
searchdesc = "This is how we are to approach the Eucharist, not just on this most Solemn day, but indeed whenever we do it: With our hearts open to reverently receive the Most Holy Body and Blood of Christ."
image = "/img/homilies/last-supper-corpus-christi.jpg"
featured = "last-supper-corpus-christi.jpg"
featuredalt = "The Last Supper - Institution of the Eucharist"
featuredpath = "/img/homilies/"
linktitle = ""
title = "The Source and Summmit of the Christian Life: Corpus Christi"
type = "post"
format = "homily"

+++

{{< audio "/audio/homily-corpus-christi-year-b.mp3" >}}  
*(Audio recorded live, 6 June 2021)*

# Readings:
## Ex. 24:3-8; Ps. 116; Heb. 9:11-15; Mk. 14:12-16, 22-26

Today we celebrate the Solemnity of the Most Holy Body and Blood of Christ, and I can think of no more significant feast day for our return to the Eucharist, the source and summit of the Christian life. For over a year, so many of our congregation have been hunkered down in their homes, attending Mass virtually from their couch churches, and celebrating spiritual communion. Now, we welcome back with great fervor all who are able to once again attend the Holy Sacrifice of the Mass and receive truly and substantially the Most Holy Body and Blood of Christ.

A couple years ago, there were some alarming statistics floating around. Among those stats was the shocking admission that 75% of Mass goers did not believe in Jesus’ Real Presence in the Most Blessed Sacrament. While that may be difficult to hear, and perhaps some of us may have even pondered this great mystery, as the poet says, *absence makes the heart grow fonder*. And it makes me wonder: Might the Lord have chosen this special feast day for all of us to return once again to him? Is it coincidence or Divine providence?

Part of our return to the Lord, then, ought to include our disposition towards him in the Most Holy Sacrament of the Altar. To prepare us for this awesome gift, allow me to impart the wisdom from the early Church. St. Cyril of Jerusalem, in his *Lectures on the Christian Sacraments*, has this to say about the Eucharist:

{{< img-post-width "/img/homilies/" "cyril-of-jerusalem.jpg" "St. Cyril of Jerusalem" "right" "30%">}}

>“That our Lord Jesus Christ the same night in which He was betrayed, took bread, and when He had given thanks He broke it, and said, Take, eat, this is My Body: and having taken the cup and given thanks, He said, Take, drink, this is My Blood. Since then He Himself has declared and said of the Bread, This is My Body, who shall dare to doubt any longer? And since He has affirmed and said, This is My Blood, who shall ever hesitate, saying, that it is not His blood?”

Cyril goes on to say that Christ once turned water into wine, is it incredible that He should have turned wine into blood? Further, could there be any more precious substance on earth than the divinity of Christ under the appearances of bread and wine? Will platinum, gold, silver, or anything else ever compete with so valuable a gift from God?

St. Cyril then offers this instruction for the faithful who approach the altar for communion:
>“Approaching, therefore, come not with your wrists extended, or your fingers open; but make your left hand as if a throne for your right, which is on the eve of receiving the King. And having hallowed your palm, receive the Body of Christ, saying after it, Amen. Then after you have carefully hallowed your eye by the touch of the Holy Body, partake thereof; giving heed lest you lose any of it; for what you lose is a loss to you as it were from one of your own members.”

{{< img-post-width "/img/homilies/" "hippolytus.jpg" "Hippolytus of Rome" "left" "30%">}}

Hippolytus, author of *On the Apostolic Tradition*, writes, “When you eat and drink, do so with integrity….For [Jesus] said, ‘You are the salt of the earth.’ And if a portion is offered to all in common, take from it….Let those who are invited to eat do so in silence, and not wrangle with words.” And perhaps, most significantly for me, “And if the faithful are present at supper in the absence of a bishop but a presbyter or deacon is present...let everyone be glad to accept a blessed portion from the hand of the presbyter or the deacon.”

This is how we are to approach the Eucharist, not just on this most Solemn day, but indeed whenever we do it: With our hearts open to reverently receive the Most Holy Body and Blood of Christ. And so, as we turn to the holiest part of Mass, to witness the miracle of Christ, transforming ordinary bread and wine truly and substantially into his flesh and blood, may our partaking of this holy meal, unite us once again with our Lord and our God. And by our communal participation, may we be gathered into one by the Holy Spirit, sustained by this precious food, the Holy Bread of Heaven, the Most Blessed Sacrament of the Altar, the Most Holy Body and Blood of Jesus Christ, our Lord.

---
*Given during the COVID-19 pandemic.*