+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "2021"]
date = "2021-07-03"
description = "Fourteenth Sunday in Ordinary Time, Year B"
searchdesc = "St. Thomas would not believe Jesus had risen unless he touched the risen Lord himself. Such incredulity continues throughout the world, even to this day, as many continue to reject Christ. But, Thomas' doubt was transformed into belief as he encounters Christ."
image = "/img/homilies/jesus-rejected-at-nazareth.jpg"
featured = "jesus-rejected-at-nazareth.jpg"
featuredalt = "Jesus is rejected from his native place"
featuredpath = "/img/homilies/"
linktitle = ""
title = "Doubting Thomas, the Rejection of Christ, & Faith"
type = "post"
format = "homily"

+++

{{< audio "/audio/homily-fourteenth-sunday-ot-year-b.mp3" >}}  
*(Audio recorded live, 3 July 2021)*

# Readings:
## Ez. 2:2-5; Ps. 123; 2 Cor. 12:7-10; Mk. 6:1-6

This morning was the Feast of St. Thomas the Apostle, also known as “Doubting Thomas.” Many might recall that among the first appearances of Jesus to the apostles, Thomas was not present, and therefore said, “Unless I see the mark of the nails in his hands and put my finger into the nailmarks and put my hand into his side, I will not believe” (Jn. 20:25b). He was unwilling to believe on the testimony of his closest friends that Jesus had indeed risen from the dead. The next time Jesus appears, however, Thomas was present, and Jesus says to him: “Put your finger here and see my hands, and bring your hand and put it into my side, and do not be unbelieving, but believe” (Jn. 20:27b). And of course, upon doing this, Thomas makes the famous statement, “My Lord, and my God!” It is the same statement that many of us make whenever the Sacred Host is elevated after the consecration. St. Gregory the Great has this to say about St. Thomas: 

>“Do you really believe that it was by chance that this chosen disciple was absent, then came and heard, heard and doubted, doubted and touched, touched and believed? It was not by chance but in God’s providence. In a marvelous way God’s mercy arranged that the disbelieving disciple, in touching the wounds of his master’s body, should heal our wounds of disbelief. The disbelief of Thomas has done more for our faith than the faith of the other disciples.

Today’s readings deal mostly with disbelief. The prophet Ezekiel is sent by God to a rebellious people. They do not believe! St. Paul struggles with rejection throughout his ministry, enduring insults, hardships, persecutions, and constraints. Such is the cruel fate of those sent by God to speak His Truth. And then we come to Jesus, the Incarnate Son of God, surely they will respect God’s heir! Yet, the people of his native place are astonished that the carpenter, the son of Mary, is doing mighty deeds. Perhaps they are astonished that someone of such lowly stature could amount to anything great at all—they were unable to see God working in and through him. They not only doubted, like Thomas, they outright took offense at him. Such is the fate of those who spread the Good News of God’s Word. Not even Jesus was able to perform any mighty deed there apart from healing a few sick people. As St. Augustine said, “Without God, I cannot; without me, God will not.”

Last week I spoke of the importance of faith, which is the proof of what can not be seen. Yet, the people of Jesus’ native place could see him, just as St. Thomas, after touching Jesus, could see him, just as we, who celebrate this Eucharist today, will see him. The challenge the world faces today is that despite all the manifestations of Christ throughout history, many still refuse to believe.

Jesus’ response to this situation is very matter of fact, he says, “A prophet is not without honor except in his native place and among his own kin and in his own house.” What native place is Jesus speaking of? Who are his own kin? And where is his own house? Is not the entire Universe his own native place? Are not we all, by nature, his own kin? Do we not find ourselves within his own house? Does it not stand to reason, then, that the rejection of Jesus would go far beyond the confines of the small village of Nazareth?

For example, last year several churches and Christian monuments were vandalized. Just this past week, in Canada, two Catholic churches were set on fire. Are these not clear indications of the rejection of Christ? Regardless of the motivations of those who would attack the Church, no one can change the past. The best we can do is learn from our mistakes and work together to create a better future.

Speaking of the future, tomorrow is Independence Day. We are going on 245 years of the American Dream, but we must remember that our freedoms did not come for free, rather, they are the result of generations of courageous men and women who fought to uphold their freedoms. It is no different for us today.

So, as we celebrate in freedom, this Holy Sacrifice of the Mass, perhaps we might consider the ways in which we are working to uphold our freedoms today, especially our freedom to worship. If the last year has been any indication of how easily certain freedoms can be sacrificed, perhaps it is all the more pressing for us to make every effort to ensure that our freedom, our independence, can never be taken from us, or from generations to come. For such an effort, we need more than determination of the will, we need Christ, the very source of freedom. It is Christ who created us in his image and likeness; it is Christ who called each of us by name; it is Christ who set us apart to spread the Good News of salvation; it is Christ who empowers us to carry out his will, even in the face of rejection. To that end, we embrace him in faith, like St. Thomas, who though doubted, came to believe. For who we behold is truly our Lord and our God, and the source and summit of our faith. Blessed indeed are we whose eyes are fixed on the Lord, pleading for his mercy.

---