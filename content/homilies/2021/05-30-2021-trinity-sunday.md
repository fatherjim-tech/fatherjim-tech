+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "solemnities", "2021"]
date = "2021-05-29"
description = "Solemnity of the Most Holy Trinity, Year B"
searchdesc = "When we pray, it is in the Spirit, through the Son, to the Father. This is the great mystery of our God, who has made his presence known to us throughout the ages."
image = "/img/homilies/trinitym.jpg"
featured = "trinity.jpg"
featuredalt = "The Most Holy Trinity - Father, Son, & Holy Spirit"
featuredpath = "/img/homilies/"
linktitle = ""
title = "Behold, I Am With You Always"
type = "post"
format = "homily"

+++

{{< audio "/audio/homily-trinity-sunday-year-b.mp3" >}}  
*(Audio recorded live, 30 May 2021)*

# Readings:
## Dt 4:32-34, 39-40; Ps. 33; Rom. 8:14-17; Mt. 28:16-20

Last week, I spoke of how there are really only two solemnities that focus our attention on the Holy Spirit. They are Pentecost, which we celebrated last Sunday, and the Solemnity of the Most Holy Trinity, which we celebrate today. That these two solemnities happen one after the other is no mistake, because to celebrate the Holy Spirit is to also celebrate God the Father, and God the Son. It is difficult to think of one without acknowledging the others. And when we pray, it is in the Spirit, through the Son, to the Father. This is the great mystery of our God, who has made his presence known to us throughout the ages.

One of my favorite passages in Matthew’s Gospel is when Jesus says: “And behold, I am with you always, until the end of the age.” How is this possible? How does Christ remain with us always? Well, for one, we have the sacraments, which are all manifestations of the Divine life in us, the most important of which is the Eucharist. We will speak more about this next week on the Solemnity of the Most Precious Body and Blood of Christ, *Corpus Christi*. But, in addition to Jesus' Eucharistic presence among us, there is also what St. John has been telling us for the last few weeks: Remain in him. If we remain in him, then he will remain in us. As Moses says, keep the commandments “that you and your children after you may prosper.” So, our participation, our acts of charity, our prayers, our staying on the vine, keep us close to our source of life.

This source is not far from us. St. Paul reminds us that we did not receive a spirit of slavery, but a Spirit of adoption. Therefore, we are all sons and daughters of God, such that we, along with Jesus, may call him *Abba*, Father. This is how close God is to us. Again, as Moses says: *What God is as close to his people as our God is to us?* For in him we live, and move, and have our being. Not a single person in this world can say they willed themselves into being, rather, it is by the love of God that we have life.

And so, as we reflect upon the great mystery that is our Triune God, may our celebration of the Eucharist today deepen our awareness of the Spirit, draw us closer to Christ, who is with us always, and lead us to the Father, today and until the end of the age.

---
*Given during the COVID-19 pandemic.*