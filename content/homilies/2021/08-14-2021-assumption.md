+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "solemnities", "2021"]
date = "2021-08-14"
description = "Sol. of the Assumption of the Blessed Virgin Mary, Year B"
searchdesc = "Mary shows us how the meek inherit the kingdom, how a life of discipleship leads us to God, how to cooperate with God’s grace, and how to respond in faith."
image = "/img/homilies/mary-assumption.jpg"
featured = "mary-assumption.jpg"
featuredalt = "Mary being assumed into heaven"
featuredpath = "/img/homilies/"
linktitle = ""
title = "Mary's Obedience, Exhalted in Heaven"
type = "post"
format = "homily"

+++

{{< audio "/audio/homily-assumption-year-b.mp3" >}}  
*(Audio recorded live, 14 August 2021)*

# Readings:
## 1 Chr. 15:3-4,15-16;16:1-2; Ps. 132; 1 Cor. 15:54b-57; Lk. 11:27-28

Today is the Solemnity of the Assumption of the Blessed Virgin Mary. In addition to being one of the most ancient celebrations of our Blessed Mother, it is also the title of our cathedral, which places the entire diocese under the patronage of Mary. The Church has celebrated this feast day under various names since about the fifth century. Perhaps the most well known alternative is the Dormition of Mary, a word describing her falling asleep. The Church teaches that while Mary did experience an earthly death, the Lord would not allow her body to undergo decay, so she was assumed into heaven where she now sits at the right hand of Christ. While this belief has been accepted throughout the Church, it was not until recently that it was promulgated as an official dogma of the Catholic Church. On November 1, 1950, Pope Pius XII defined the Assumption of Mary to be a dogma of faith. Dogma’s are the non-negotiable tenets of belief, as opposed to doctrine, which is more fluid and can develop over time. Pope Pius XII stated, “We pronounce, declare and define it to be a divinely revealed dogma that the immaculate Mother of God, the ever Virgin Mary, having completed the course of her earthly life, was assumed body and soul to heavenly glory” (Munificentissimus Deus, 44).

This seems quite logical given the teachings of Christ and his desire that where he is we also may be. After Mary’s earthly life was complete, would not the Lord take his Blessed Mother to himself? It seems only proper that the one who heard the Word of God and observed it would be caught up to God; it seems only proper that the one who, full of grace, carried the author of life within her womb, would be caught up to God; it seems only proper that the one who, at the foot of the cross, was entrusted as Mother of the Church, would be caught up to God; and it seems only proper that the one who models for us the pinnacle of femininity, the life of chastity, virginity, and grace, would be caught up to God.

At the heart of Mary’s life is her obedience to God’s will. St. Maximilian Mary Kolbe, priest and martyr who gave his own life in place of another in the Nazi death camps during WWII, had this to say about Mary’s obedience to God’s will:

>“It is beyond all doubt that Mary’s will represents to us the will of God himself. By dedicating ourselves to her we become in her hands instruments of God’s mercy even as she was such an instrument in God’s hands. We should let ourselves be guided and led by Mary and rest quiet and secure in her hands. She will watch out for us, provide for us, answer our needs of body and spirit; she will dissolve all our difficulties and worries.”

Mary shows us how the meek inherit the kingdom; how a life of discipleship leads us to God; how to cooperate with God’s grace; and how to respond in faith. This is why she is blessed among women, exalted as Mother of the Most High God, and venerated as most blessed among the saints. And as Christ has taken flesh through Mary, so too do we, through Mary, attain Christ. As Mary is the Mother of Jesus, our brother, so too, is Mary Mother of the Church, and we honor her and venerate her as her children on earth.

And so, as we prepare to celebrate the Eucharist during this Solemnity in honor of Mary’s Assumption into heaven, may the communion we receive help us to remain in Christ. And may we, who like Mary, become tabernacles of Christ within us, remain faithful, like Mary, to the very end, so that we too may be caught up with Christ and enter into heavenly glory.

---