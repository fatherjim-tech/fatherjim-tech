+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "advent", "2021"]
date = "2021-12-18"
description = "Fourth Sunday of Advent, Year C"
searchdesc = "It is appropriate for us to hear about Mary’s visit with her cousin Elizabeth, especially this time of year. Most of us are preparing to visit family or have family visit us for Christmas. Is coronavirus an obstacle to being with loved ones? How can we get creative about including those family members whom we love?"
image = "/img/homilies/mary-elizabeth-visitation.jpg"
featured = "mary-elizabeth-visitation.jpg"
featuredalt = "Mary visits Elizabeth, her cousin"
featuredpath = "/img/homilies/"
linktitle = ""
title = "Removing Obstacles to Embrace One Another"
type = "post"
format = "homily"

+++

{{< audio "/audio/homily-third-sunday-advent-year-c.mp3" >}}  
*(Audio recorded live, 18 December 2021)*

# Readings:
## Mi. 5:1-4a; Ps. 80; Heb. 10:5-10; Lk. 1:39-45

The prophet Micah describes the humble origins of the one who is to be ruler in Israel, Bethlehem. The word “Bethlehem” literally means “bread basket,” which is fitting considering the sign of bread Jesus gave for us to remember him. Whenever we gather together for the Eucharist, we remember Jesus. And we gather as the People of God—those who await the blessed hope of Jesus’ reign of peace. And that truly is what we seek: peace. Peace from wars, peace from arguments, peace from violence, peace from sickness, peace from suffering. All these things we seek whenever we come to the table of the Lord.

But, we are sometimes all too aware of our own weakness, all too aware of our sins. We pray with the psalmist who begs the Lord to make us turn to him, to let us see his face and be saved. Our gathering here is a time of closeness with the Lord, a time of unity in faith, a time of visitation with the One who came to visit us when he was born in Bethlehem.

Jesus came to receive a body from us so that he might destroy sin once for all. He accomplished this by his obedience to the Father’s will. And while he suffered, he did so knowing that he was freeing us all from the clutches of the Evil One who seeks to drag us down, make us despair, and destroy our life. I often think the real sickness we are dealing with in the world today is a lack of faith in Almighty God. If we wish to be free of our pain and suffering, our anxieties and woes, we need God. Who knows us better than the One who created us? Who knows our heart better than the One who is Love? This love was poured out for us and for many for the forgiveness of sins, to reconcile us once again to the Father, estranged as we were from our own selfish pursuits. And though we may stray from time to time, he calls us back to himself. The cross transforms our thinking because we see not a symbol of death and destruction, but rather the tree of life, renewing us in the eternal hope that where Jesus is we also may be.

And so we journey. As Mary journeyed to see her cousin Elizabeth. The angel Gabriel said to her, “behold, Elizabeth, your relative, has also conceived a son in her old age, and this is the sixth month for her who was called barren; for nothing will be impossible for God” (Lk. 1:36-37). The word “behold” means to “hold in view.” So, the angel was telling Mary to go and see for herself this great miracle the Lord has made for her cousin Elizabeth.

It is appropriate for us to hear about Mary’s visit with her cousin Elizabeth, especially this time of year. Most of us are preparing to visit family or have family visit us for Christmas. These familiar gatherings are what add to the beauty of the Season. Sadly, we are all too aware of the reports of contagious variants and obstacles standing in the way. Satan is always opposing our gathering together, especially in the name of Jesus. But, take courage, Jesus has already defeated him. He has no power over us other than the power we give him. We do well to evaluate the obstacles in our lives. Are we putting up unnecessary barriers? Are we isolating due to fear? Have we allowed the devil undue influence over our lives?

We have our Advent Penance Service at 4PM this Sunday. Perhaps it will be a good opportunity to examine those areas in our lives that have been under the influence of evil. In the meantime, perhaps we might reflect on the upcoming visits with family. Is coronavirus an obstacle to being with loved ones? How can we get creative about including those family members whom we love? Mary went on several journeys while pregnant without a thought of any obstacles. She even gave birth to Jesus in a cave—far from the most sterile location to have a baby. Nevertheless, she made it work, she carried out the Father’s will.

So, may our gathering today strengthen our faith that God will provide and help us to get creative for how we might spend time with family. We need each other now more than ever. So, may the unity we share here in Christ spread to our homes and beyond, and may the love of God guide us like Mary to that joyful embrace of our relatives as we welcome the Price of Peace into our hearts.

---