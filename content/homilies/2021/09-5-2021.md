+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "2021"]
date = "2021-09-05"
description = "Twenty-Third Sunday in Ordinary Time, Year B"
searchdesc = "Two themes run through our readings today: Restoration and being opened. Restoration will come—just as it came for the Israelites, just as it came for the deaf mute man, just as it comes for us whenever we gather at the altar of sacrifice."
image = "/img/homilies/jesus-heals-deaf-man.jpg"
featured = "jesus-heals-deaf-man.jpg"
featuredalt = "Jesus heals the deaf mute man"
featuredpath = "/img/homilies/"
linktitle = ""
title = "Be Opened to the Lord Who Renews Us"
type = "post"
format = "homily"

+++

{{< audio "/audio/homily-twenty-third-sunday-ot-year-b.mp3" >}}  
*(Audio recorded live, 5 September 2021)*

# Readings:
## Is. 35:4-7a; Ps. 146; Jas. 2:1-5; Mk. 7:31-37

There are some beautiful themes running through our readings today. In the gospel, Jesus says to the deaf mute man, “Ephphatha! … Be opened!” I find this to be a timely command, not just in the context of the gospel, but for us today. Being open to the Word of God can sometimes be a challenge, even for those who can hear. We are so bombarded by the media and information it can make our head spin. Somewhere in the midst of all that noise there has to be a voice that makes sense! Of course there is. That voice is Jesus. That voice is always Jesus, who is the reason we are all gathered here today.

Our pastor mentioned in his parish letter that Mass attendance has been decimated due to the pandemic. “Decimated” is a strong word, but when we look around here, we see only about half as many people as before. Certainly not all of them died of COVID, but we know many are afraid. We have taken great care to ensure that our church is as safe as possible throughout this struggle, and I hope that has given everyone piece of mind. But, the best encouragement any of us can receive comes from God.

The Lord tells the prophet Isaiah to say to the hearts of those who are frightened: “Be strong, fear not! Here is your God, he comes with vindication; with divine recompense he comes to save you.” I know that the last eighteen months have been difficult for us all, but as we place our faith and trust in the Lord, we should be confident that he will make all things right.

Some of us may have gotten sick, some of us may have lost a loved one, some of us may be struggling, but God wants all of us to know that restoration will come—just as it came for the Israelites, just as it came for the deaf mute man, just as it comes for us whenever we gather at the altar of sacrifice. When we celebrate the Eucharist, it is as if Jesus is saying to each of us, *Be opened! Receive me into your heart and I will give you rest*. Jesus renews our spirit every week. He is our primary source of peace in a world full of strife.

So, as we prepare to receive Jesus into our hearts, let us take courage in knowing that he is with us always, even until the end of time. And may the communion we share help us to be opened to his grace, his love, his peace, so we may go forward confidently praising the Lord in our soul.

---