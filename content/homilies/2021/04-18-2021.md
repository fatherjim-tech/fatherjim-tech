+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["easter", "sundays", "2021"]
date = "2021-04-18"
description = "Third Sunday of Easter, Year B"
searchdesc = "In Luke’s Gospel, when Jesus appears, he appears to them all and invites them all to touch him and know that he is not a ghost, but truly and substantially present in their midst."
image = "/img/homilies/jesus-appears-upper-room.jpg"
featured = "jesus-appears-upper-room.jpg"
featuredalt = "Jesus appears in the upper room"
featuredpath = "/img/homilies/"
linktitle = ""
title = "The Apostles Testify to What They Have Seen & Touched"
type = "post"
format = "homily"

+++

# Readings:
## Acts 3:13-15, 17-19; Ps. 4; 1 Jn. 2:1-5a; Lk. 24:35-48

{{< audio "/audio/homily-third-sunday-easter-year-b.mp3" >}}  
*(Audio recorded live, 18 April 2021)*

Last week, we heard about doubting Thomas, who insisted that unless he touch the wounds of the Risen Lord, he would not believe. Then, when Jesus appears and invites him to touch his wounds, Thomas exclaims, “My Lord and my God!” Today’s Gospel gives us a similar account, only without singling out Thomas. In Luke’s Gospel, when Jesus appears, he appears to them all and invites them all to touch him and know that he is not a ghost, but flesh and bone. Jesus even goes so far as to eat in front of them to prove that he is truly and substantially present in their midst. It is because of these resurrection accounts with Jesus that the Apostles have been able to testify to what they had seen, what they had heard, what they had touched with their own hands.

St. Peter proclaims the resurrection of Christ in the Temple, saying “The author of life you put to death, but God raised him from the dead; of this we are witnesses.” He was speaking to many of the people who were responsible for Jesus’ death. But, Peter does not condemn them. He instead says, “Now I know, brothers, that you acted out of ignorance, just as your leaders did….” In other words, they were led astray by their wicked leaders who twisted the truth. But, Peter seeks to reconcile them to the Father by exhorting them and encouraging them to repent and be baptized, that their sins may be wiped away.

Peter testifies to the truth, and the truth we have all come to accept in faith is that Jesus Christ is risen. For this we rejoice and are glad. And knowing this gives us confidence to keep his commandments so that we, too, may share in his resurrection. As St. John says, “Those who say, ‘I know him,’ but do not keep his commandments are liars, and the truth is not in them. But whoever keeps his word, the love of God is truly perfected in him.”

The call of the Christian is to be one who keeps the commandments, who walks in the law of love, who desires to be with Christ in eternity and relies on the grace he bestows on us throughout this life. Maybe there was a time in our life when we were ignorant like the people in Jerusalem, but now we know that we have an Advocate with the Father who intercedes on our behalf.

And so, mindful of the paschal sacrifice of Christ and with longing for his coming again, let us turn to the Eucharist with open hearts and minds to receive him in the Most Blessed Sacrament. And while we often tend to think of the distance between heaven and earth, may this holy meal be a reminder of how close heaven truly is whenever we are gathered in Jesus name.

---
*Given during the COVID-19 pandemic.*