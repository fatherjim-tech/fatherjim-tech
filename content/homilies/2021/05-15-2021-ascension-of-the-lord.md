+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "easter", "solemnities", "2021"]
date = "2021-05-15"
description = "Seventh Sunday of Easter, The Ascension of the Lord, Year B"
searchdesc = "Jesus’ Ascension is not so much a departure from the world, as a return to his place at the right hand of the Father. By ascending to the Father, Jesus prepares to send the Holy Spirit upon his disciples on Pentecost."
image = "/img/homilies/jesus-ascending.jpg"
featured = "jesus-ascending.jpg"
featuredalt = "Jesus Ascending into the Clouds"
featuredpath = "/img/homilies/"
linktitle = ""
title = "Jesus Ascends, But Remains With Us In Spirit"
type = "post"
format = "homily"

+++

{{< audio "/audio/homily-ascension-year-b.mp3" >}}  
*(Audio recorded live, 15 May 2021)*

# Readings:
## Acts 1:1-11; Ps. 47; Eph. 1:17-23; Mk 16:15-20

Today, we celebrate the Solemnity of the Ascension of the Lord. This feast is traditionally observed forty days after Easter Sunday, which is why it has come to be known as Ascension Thursday. The bishops of NJ all agreed due to the restrictions still in place that the feast be moved to the following Sunday. Nevertheless, the fact that the feast would normally be celebrated forty days after Easter is not without significance. As you know, the Season of Lent is forty days of penance and prayer leading up to the Triduum. The Church then observes another forty days of joyful celebration in the days leading up to Pentecost. The first forty days of sorrow give way to a second forty days of joy. To the disciples, their mourning turned into joy as Christ rose from the dead and remained with his disciples to prepare them for their true mission.

Today, we celebrate Jesus’ Ascension not as a departure from the world, but as a return to his place at the right hand of the Father. As he reminds us in the Gospel of John: “For if I do not go, the Advocate will not come to you. But if I go, I will send him to you.” St. Paul tells us that the honor bestowed upon Jesus places him “far above every principality, authority, power, and dominion, and every name that is named not only in this age but also in the one to come.” By ascending to the Father, Jesus prepares to send the Holy Spirit upon his disciples on Pentecost.

Our First Reading from the Acts of the Apostles captures the promise Jesus makes to his disciples as he says, “in a few days you will be baptized with the Holy Spirit.” This promise of Jesus foreshadows the descent of the Holy Spirit on Pentecost, which we will celebrate next Sunday. It is the arrival of the Spirit that gives the disciples the courage to leave the upper room and to begin proclaiming the Good News of Jesus Christ throughout the world. As followers of Jesus, we also receive the Holy Spirit, especially when we gather together as God’s People, and most especially when we celebrate the Sacraments. In a special way, this past year, the Spirit has remained with us, and we are never alone.

So, today, as we reflect upon the glorious ascension of our Blessed Lord, we are mindful of the way in which He remains with us in Spirit. And as we begin to round the corner with the pandemic, we continue to pray for healing and strength in the days ahead. May all who receive Jesus, in the Most Blessed Sacrament, be filled with his Holy Spirit, and may our cooperation with the grace of God help us to carry out Jesus’ command, to: “Go into the whole world and proclaim the gospel to every creature.”

---
*Given during the COVID-19 pandemic.*