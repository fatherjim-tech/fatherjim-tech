+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["lent", "holy-week", "sundays", "2021"]
date = "2021-03-28"
description = "Sunday of the Passion of the Lord, Year B"
searchdesc = "Jesus is the fulfillment of what has been foretold by the Scriptures."
image = "/img/homilies/jesus-crucified-centurian.jpg"
featured = "jesus-crucified-centurian.jpg"
featuredalt = "Jesus"
featuredpath = "/img/homilies/"
linktitle = ""
title = "This Man Truly Is the Son of God"
type = "post"
format = "homily"

+++

# Readings:
## Is. 50:4-7; Ps. 22; Phil. 2:6-11; Mk. 15:1-39

{{< audio "/audio/homily-palm-sunday-year-b.mp3" >}}  
*(Audio recorded live, 28 February 2021)*

While it is difficult to hear about our Lord’s Passion, crucifixion, and death, the focus of our readings, and indeed Mark’s Gospel, is how Jesus is the fulfillment of what has been foretold by the Scriptures. Our readings all speak, in one way or another, of Jesus’ destiny. The prophet Isaiah foretells the way in which the Suffering Servant will offer himself in expiation for their sins, saying, “I gave my back to those who beat me, my cheeks to those who plucked my beard; my face I did not shield from buffets and spitting.” The Psalmist echoes this, “All who see me scoff at me...They have pierced my hands and my feet; I can count all my bones.” And in our Gospel, Jesus himself exclaims this psalm from the cross, saying, “My God, my God, why have you abandoned me?” Each passage, in their own way, reveal something about Jesus’ destiny.

It is easy for us to get caught up on the emotional state of Jesus during his crucifixion and death. After all, it was one of the most gruesome, brutal deaths anyone could suffer at the time. Nevertheless, the reason Mark highlights Jesus praying Psalm 22 is to show how this psalm is fulfilled by Jesus on the cross. In other words, it is to say that the Scriptures foretold this all along and now has come to pass in Jesus. In fact, if we were to read to the end of Psalm 22, we would see that despite the lamentation, “My God, my God, why have you abandoned me,” it is nevertheless a psalm of absolute trust in God: “All the ends of the earth will remember and turn to the LORD; All the families of nations will bow low before him.” Whenever the Old Testament speaks of nations, they are referring to Gentile territories. And when the centurion, a Gentile, saw how Jesus breathed his last he said, “Truly this man was the Son of God!” Now is the time of fulfillment. The kingdom of God is at hand. Salvation has come to the world and the victory has been won for us by Jesus. “Because of this, God greatly exalted him and bestowed on him the name which is above every name, that at the name of Jesus every knee should bend, of those in heaven and on earth and under the earth, and every tongue confess that Jesus Christ is Lord, to the glory of God the Father,” as we do today.

The communion we share, therefore, is a sign of the sacrifice of Christ, who was not abandoned by the Father, but rather, who trusted his life entirely to the Father, and now sits at the Father’s right hand in glory. We too have a share in the glory of Christ as we conform our will to the will of the Father. So, this Passion Sunday, may our celebration of the Holy Sacrifice of the Mass give us the strength and courage of Jesus, who shows us the way to the Father.

---
*Given during the COVID-19 pandemic.*