+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "2021"]
date = "2021-07-18"
description = "Sixteenth Sunday in Ordinary Time, Year B"
searchdesc = "Our Gospel shows us exactly how Jesus fulfills the prophecy of Jeremiah. Jesus is the Son of David, the Lord of Justice, the Prince of Peace. He is our shepherd."
image = "/img/homilies/good-shepherd-bryan-ahn.jpg"
featured = "good-shepherd-bryan-ahn.jpg"
featuredalt = "Jesus, the Good Shepherd, nourishing his flock"
featuredpath = "/img/homilies/"
linktitle = ""
title = "Jesus is the Lord of Justice, our true Shepherd"
type = "post"
format = "homily"

+++

{{< audio "/audio/homily-sixteenth-sunday-ot-year-b.mp3" >}}  
*(Audio recorded live, 18 July 2021)*

# Readings:
## Jer. 23:1-6; Ps. 23; Eph. 2:13-18; Mk. 6:30-34

Today’s passage from the Book of Jeremiah is a prophecy against those rulers, those shepherds who mislead and scatter the people, God’s flock. Jeremiah prophesied during the time of the Babylonian Exile and the message he gives today is one of hope and restoration. *The Lord will raise up a righteous shoot to David*, a king to govern wisely and restore justice in the land. Who might this Son of David be? The one they will call, “The LORD our justice.”

*The Lord is my shepherd, there is nothing I shall want*. He is the one who anoints us, spreads the table before us, and protects us in the dark valley. With our Lord, we shall fear no evil. St. Paul speaks of the way in which Jesus reconciles us with God. He does this for all people, while preaching peace, saying, “Peace I leave with you; my peace I give to you” (Jn. 14:27). This peace comes to us, even in our darkest moments, because he protects us with his rod and his staff. He is the Lord, my shepherd, my rock, my fortress.

Our Gospel shows us exactly how Jesus fulfills the prophecy of Jeremiah. Mark says, “When [Jesus] disembarked and saw the vast crowd, his heart was moved with pity for them, for they were like sheep without a shepherd; and he began to teach them many things.” How else does the restoration of justice and right come but through proper teaching? Jesus not only tends to his flock with mighty deeds of healing and casting out demons, he also nourishes them with his teaching.

The beauty of the Catholic faith is that looks not to miracles for comfort and peace, but rather, to the constant renewal of the mind. That can only happen if we are being nourished by the truth. So much of what we are fed by other sources is muddied with lies or partial truthes. It is so easy to be misled today. But, to those who seek truth, justice, and right, they need look no further than Jesus Christ, who is himself the way, the truth, and the life. The disciples learned this while following Jesus; they embraced it while they were on mission; and after Jesus ascended, they lived according to “the way” of Jesus, spreading his Word throughout the world.

So, as we continue to reflect upon the fulfillment of God’s promise to Jeremiah, the gift of Jesus Christ, the Son of David, our Good Shepherd, the Lord of Justice, the Prince of Peace, may the communion we share unite us as one flock. And may we rely ever more faithfully on Christ, who draws us together, who teaches us many things, and gives us his peace.

---