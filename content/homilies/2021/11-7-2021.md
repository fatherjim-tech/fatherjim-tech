+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "ordinary-time", "2021"]
date = "2021-11-07"
description = "Thirty-Second Sunday in Ordinary Time, Year B"
searchdesc = "Jesus did not enter into a sanctuary, a tabernacle, that was made by hand, a copy of the true one, but rather, by his death, he entered into heaven itself. The resurrection and ascension of Jesus is the proof of this awesome reality. And so, Jesus is the High Priest, and his priesthood is eternal."
image = "/img/homilies/jesus-last-supper-eucharist.jpg"
featured = "jesus-last-supper-eucharist.jpg"
featuredalt = "Jesus as High Priest instituting the Eucharist at the Last Supper"
featuredpath = "/img/homilies/"
linktitle = ""
title = "The Priesthood of Jesus: An Awesome Vocation"
type = "post"
format = "homily"

+++

{{< audio "/audio/homily-thirty-second-sunday-ot-year-b.mp3" >}}  
*(Audio recorded live, 7 November 2021)*

# Readings:
## 1 Kgs. 17:10-16; Ps. 146; Heb. 9:24-28; Mk. 12:38-44

This Sunday we begin Vocation Awareness week and I would like to speak a bit about the vocation of priesthood. God calls each of us by name. This, of course, began with our baptism, when our parents announced our names to the Church’s minister and we were claimed for Christ. But, what about the rest of our life? Does God call us at baptism and that’s it? Or does he continue to call out to us, as a shepherd calls to his sheep? I think we all know the answer to that question. God is always calling us, but the question is: Are we listening? Do we hear God’s call?

In the Letter to the Hebrews, the author establishes that Christ is the High Priest, and that priesthood precedes the Levitical priesthood, therefore, it is of a higher order. We heard that Jesus did not enter into a sanctuary, a tabernacle, that was made by hand, a copy of the true one, but rather, by his death, he entered into heaven itself. The resurrection and ascension of Jesus is the proof of this awesome reality. And so, Jesus is the High Priest, and his priesthood is eternal. But, what about we priests on earth?

How is the priesthood of Jesus different than the Levitical priesthood? Well, in addition to Jesus’ having passed through the veil and entered into his heavenly glory, he has given his apostles a special gift to carry out his mission on earth, so that what he began while on earth would continue until the end of time. The apostles, through their eyewitness, their preaching, their teaching, their ministering, and celebrating the sacraments, participate in the mission of Jesus—they carry on what Jesus had already started.

The apostles were not the only ones graced with this mission, indeed all the baptized have been called to help build the church on earth so that the words of Jesus may spread far and wide, and all nations may come to know the goodness of the Lord. So, here we are 2,000 years later and still preaching the words of Jesus.

This gift of priesthood is unique in that the priest conforms his own life to the life of Christ. He does so by making sacrifices on behalf of others. The priest gives up a wife and children so that he may minister freely to his adopted children in faith. And as Jesus said, “Amen, I say to you, there is no one who has given up house or brothers or sisters or mother or father or children or lands for my sake and for the sake of the gospel who will not receive a hundred times more now in this present age: houses and brothers and sisters and mothers and children and lands, with persecutions, and eternal life in the age to come.” In other words, by giving up those things myself, I have received them back a hundred fold in you. You are my brothers and sisters and mothers and children. I won’t say anything about the persecutions.

Since I have been ordained to the priesthood, I have been abundantly blessed, not because I deserve it, but because of the people God has placed into my life. I have celebrated baptisms, weddings, blessings, reconciliations, funerals, and most importantly, the Eucharist. There is no more humbling experience on earth than to be a man holding the Creator of the Universe in his own two hands. And when I say, as the centurion did, “Lord, I am not worthy that you should enter under my roof...” that is ultimately why I am here—not for myself, not for personal gain, but so all of you, my parish family, may receive Jesus in the Most Blessed Sacrament. That I may be for each of you a bridge to Christ, a conduit for healing, a shoulder to lean on, someone to laugh with, or a friend to rely on.

Truly, the priest is not his own, for he is the servant of all. If you wish to be first, then strive to be last. Perhaps there are some young men in our congregation who have a desire to be of service. Maybe that manifests in the way you help your family or the kinds of jobs you have had; maybe you have a genuine concern for the good of others. Is God calling you to be of service in another way? Is God calling you to be a priest? This is a good time to pray and ask the Lord, “Are you calling me?"

For the rest of us, if you know someone who you think might make a good priest, maybe say something to them or fill out one of the little cards we have available. Above all, pray for vocations. Pray that the voice of Jesus may be heard by those whom he is calling and that they may be open to his call. And now, as we prepare to celebrate the Eucharist, may the communion we share unite us in the perfect bond of love and friendship that Christ has called each of us to share.

---