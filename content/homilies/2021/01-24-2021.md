+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["ordinary-time", "sundays", "2021"]
date = "2021-01-24"
description = "Third Sunday of Ordinary Time, Year B"
searchdesc = "What evils have I grown accustomed to? Is there something I can turn away from? Is there something standing between me and listening to the Word of God?"
image = "/img/homilies/jesus-pointing-to-heaven.jpg"
featured = "jesus-pointing-to-heaven.jpg"
featuredalt = "Image of John the Baptist points to Jesus, the Lamb of God"
featuredpath = "/img/homilies/"
linktitle = ""
title = "Sunday of the Word of God"
type = "post"
format = "homily"

+++

# Readings:
## Jon. 3:1-5, 10; Ps. 25; 1 Cor. 7:29-31; Mk. 1:14-20

{{< audio "/audio/homily-third-sunday-ot-year-b.mp3" >}}  
*(Audio recorded live, 24 January 2021)*

Today, this third Sunday in Ordinary time is now known as Sunday of the Word of God. Pope Francis instituted this particular Sunday last year to promote a closer relationship with holy Scripture, which contains the word of God. How often do we reflect on the awesome significance that God has inspired so many sacred authors to write down His words so that they may be handed on from generation to generation? That the word of God has been copied again and again throughout the ages, was translated into dozens of different languages, and is still being proclaimed, read, and understood by us today—that, in and of itself, is a miracle. So, let us break open this miracle to appreciate what the Word of God wants to tell us.

The last couple weeks, we have heard two accounts of the Baptism of the Lord, and this week we see the beginning of Jesus’ public ministry. The passage we heard today contains the first words Jesus speaks in the Gospel of Mark: “This is the time of fulfillment. The kingdom of God is at hand. Repent, and believe in the gospel.” That these words happen to match up with Sunday of the Word of God seems quite providential. So, what might the Word be saying to us about fulfillment, the kingdom of God, repentance, and belief in the gospel?

If we were to tie in our first reading from the Book of the Prophet Jonah, we might consider that Jonah was a disobedient prophet. When God had first commanded Jonah to go to the city of Nineveh, he refused and ran away. His disobedience led him to take a boat out to sea, but when a fierce storm came, the others in the boat realized that one of them must have offended God. Jonah decides to jump overboard, but rather than perish at sea, he is swallowed by a whale and three days later spit out just outside the city of Nineveh. It would seem the whale was more obedient to God’s word than Jonah! Nevertheless, Jonah finally acquiesces and enters Nineveh to prophecy God’s word of warning to the people, saying, that if they do not repent of the evil they were doing, the city would be destroyed. And before Jonah got one-third of the way through the city, the scriptures tell us, the people of Nineveh believed the word of God, proclaimed a fast, and repented. Hearing the Word of God causes the people to turn away from the evil they had grown accustomed to. So, we might ask ourselves, what evils have I grown accustomed to? Is there something I can turn away from? Is there something standing between me and listening to the Word of God?

Jesus says, “The kingdom of God is at hand. Repent and believe in the gospel.” Now, repentance tends to sound negative, but is it really a negative? Perhaps it would help to look more closely at the word repent. The word itself comes from the Greek word *metanoia*, which is a combination of two words: *meta*, meaning “after” or “behind,” and *noeo*, meaning “to think or perceive.” In other words, *metanoia* means “to think differently after,” but after what? Well, in the context of the prophet Jonah and our Gospel, to think differently after encountering the Word of God. *Now is the time of fulfillment.*

By encountering the Word of God, the people of Nineveh turned from the evil they were doing and their city was spared from destruction; by encountering the Word of God, Simon, Andrew, James, and John, left their former professions in pursuit of a different kind of fishing; by encountering the Word of God here, we begin to think differently, to be enlightened and transformed by his grace and strengthened in our conviction for his Truth.

What is his Truth? *Now is the time of fulfillment. The kingdom of God is at hand.* So, may our encounter with the Word of God today lead us to think differently afterward, to change our life for the better, to embrace Christ all the more. And may the communion we share today make us one with Christ; one with each other; one with all people throughout the ages who listened to the Word of God and thought differently after.

---
*Given during the COVID-19 pandemic.*