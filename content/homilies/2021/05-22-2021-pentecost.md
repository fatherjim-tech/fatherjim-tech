+++
author = "Fr. Jim"
categories = ["homilies"]
tags = ["sundays", "easter", "solemnities", "2021"]
date = "2021-05-22"
description = "Solemnity of Pentecost, Mass During the Day, Year B"
searchdesc = "St. Paul reminds us, “There are different kinds of spiritual gifts but the same Spirit….” We are all blessed in our own unique way by God."
image = "/img/homilies/pentecost-upper-room.jpg"
featured = "pentecost-upper-room.jpg"
featuredalt = "Holy Spirit descends as tongues of fire"
featuredpath = "/img/homilies/"
linktitle = ""
title = "As the Father has sent me, so I send you"
type = "post"
format = "homily"

+++

{{< audio "/audio/homily-pentecost-year-b.mp3" >}}  
*(Audio recorded live, 23 May 2021)*

# Readings:
## Acts 2:1-11; Ps. 104; 1 Cor. 12:3b-7, 12-13 (Gal. 5:16-25); Jn. 20:19-23

Today, we celebrate the Solemnity of Pentecost, also known as the birthday of the Church. Our readings all focus of the presence of the Holy Spirit. The Psalm speaks of the ways in which the Spirit brings life wherever it goes, constantly renewing the face of the earth. In the Gospel, the resurrected Jesus breathes the Holy Spirit upon the disciples saying, “As the Father has sent me, so I send you.” In the Acts of the Apostles, we hear of how the disciples receive the Holy Spirit, who enables their preaching to be understood by people from all over the world. And St. Paul reminds us that the only way any of us can say that Jesus is Lord is by the Holy Spirit.

Very few liturgies focus our attention on the Holy Spirit save for Pentecost and perhaps the Feast of the Holy Trinity, yet upon further reflection, we come to realize that every liturgy we celebrate, every prayer we make, every act of charity or good that we do, happens in the Spirit. As St. Paul says, “For in Him we live and move and have our being.” It is by the Holy Spirit that we were given life; it is by the Holy Spirit that we were reborn through Baptism as children of God; it is by the Holy Spirit that we are called to worship; it is by the Holy Spirit that we are caught up in the love of God.

As we heard on a previous Sunday: God is love. And anyone who loves is of God. This love has its origin in the Father, spills over through the Son, and proceeds in the Spirit to all Creation, becoming manifest through our own acts of love and kindness. Christ was sent to be a model of that love for us, showing us the way to the Father. If we desire to grow in love, then we ought to imitate Christ.

At the heart of imitating Christ is a participation in his own mission of love. It is a three-fold mission, which he exercised himself as priest, prophet, and king. We are not just called, but also sent, like the Apostles, to carry out this mission in our own lives. As priests, through our own acts of prayer, piety, and worship; as prophets, through our own study of the faith and bearing witness to others; and as kings, through our own discipline and mastery over our lives and affairs. All these are to be ordered to Christ, modeled on his teaching and example, and rooted in love.

St. Paul reminds us, “There are different kinds of spiritual gifts but the same Spirit….” We are all blessed in our own unique way by God. Our goal, then, should be to identify those gifts and to use them in such a way as to give glory to God. It all begins with the Spirit, extends to others in the Spirit, and raises the whole Mystical Body of Christ to life. The coming months and years will be a time of renewal for the Church. As more and more people find their way back to Mass and more opportunities for deepening our faith become available, may the Lord send forth his Spirit upon us, strengthening our hearts, and renewing the face of the earth. And may our celebration of the Eucharist today, make us ever mindful of Jesus’ enduring presence among us, nourishing us from within so that we may go forth loving one another and proclaiming the Good News of God.

---
*Given during the COVID-19 pandemic.*