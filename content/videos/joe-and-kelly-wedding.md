+++
type = "videos"
date = "2020-07-18"
title = "Marriage of Kelly & Joe"
description = "Highlights From Their Wedding Day"
searchdesc = "This wedding was a breath of fresh air amid one of the most challenging times in recent history. Joe and Kelly were not only meant for one another, their marriage is a beacon of hope for a future as bright as the love they share."
image = "/img/videos/kelly-and-joe-thumb.jpg"
featured = "kelly-and-joe-thumb.jpg"
featuredpath = "/img/videos/"
featuredalt = "Thumbnail of Kelly & Joe's hands embracing and wedding rings"
categories = ["videos"]
tags = ["2020", "weddings"]
author = "Fr. Jim"
format = "video"
linktitle = "Youtube Channel"
link = "https://www.youtube.com/channel/UC3lhp0Xsr22O-RFjcBQZJOA"
+++

<div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;">
  <iframe src="/video/kelly-and-joe-kellet.mp4" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; border:0;" allowfullscreen title="Kelly & Joe"></iframe>
</div>

---

{{< img-post-width "/img/videos/" "kelly-and-joe.jpg" "text" "right" "35%" >}}

At a time when the world has been fraught with social distancing, division, and strife, this wedding was a welcome respite from all the chaos. Even though most of our wedding preparation was done via video conferencing, it was an absolute joy to walk with Kelly and Joe and witness them declare their love for one another.

In one of our meetings, I must have mentioned that I was into photography, and as a token of their appreciation, they gave me a Polaroid camera! When I was little, I used to play with my mom's Polaroid; now I can continue to take instant photos like the one I took before they exchanged their vows.

Congratulations Kelly & Joe! *Ad multos annos!*

Below is a complete copy of my homily, parts which were featured throughout the video clip. Special thanks to Kelly & Joe for permission to share this here.

---

# Readings:
## Gn. 1:26-28, 31a, 1 Cor. 12:31-13:8a; Jn. 15:9-12

Kelly and Joe, here we are on your wedding day, a day you have been journeying towards for, whether you knew it or not, most of your life. Many of us know that you have been friends from middle school, high school, and through college; many of us watched as your friendship grew into a deep love; and many of us would agree that if there were a couple you could just look at and say, “they’re just right for each other,” that couple is you. So, I know that I am not alone when I say that this day, your wedding day, is not just one of expectation, but of fulfillment. And we are all so very happy for you.

One of the added bonuses of celebrating a wedding during a pandemic is the awkward reality that you will never forget this day. How’s that for making a positive out of a negative? But, it’s true. None of us will forget this time, what many have dubbed “upside down” time, but in a special way, none of us will forget the great day of joy we are celebrating because of your love for one another. In a world that has been focused so heavily on chaos, destruction, and division, you have come here today to declare your love for one another before God and the Church, and it is a breath of fresh air.

The love you share is a gift from God, and your expression of love for one another is a sharing in God’s love. And that love is expressed as the total gift of self. Just as Jesus gave his life for the sake of the world, so too, will you be expected to give of your life to one another. At the heart of your love is the true meaning of sacrifice. This is what St. Paul is getting at in his letter to the Corinthians. He reflects on the sacrifice of Jesus and concludes that everything Jesus did was out of love. And then he gives us the great definition of love: “Love is patient, love is kind. It is not jealous, is not pompous, it is not inflated, it is not rude, it does not seek its own interests….[rather,] It bears all things, believes all things, hopes all things, endures all things. Love never fails.” These words of St. Paul are words for every Christian to live by; these words of St. Paul are words for every married couple to live by. May these words bring you comfort and peace for the rest of your years together.	

In the Gospel, Jesus says to his disciples: “Remain in my love.” And the way in which we remain in his love is to keep his commandments. The reason for this is simple, he says, “I have told you this so that my joy might be in you and your joy might be complete.” In other words, if we are to be truly joyful, then we need to live a certain way, by a certain ethic, by the law of love. Now, I don’t want to get overly romantic about the law of love. Married life is not all sunshine and daisies, as I’m sure many will tell you, but it’s in those moments of struggle that you will experience joy: the joy of knowing that you’re not alone, the joy of knowing that you have someone to lean on, the joy of knowing that no matter what, you can rely on the love you share.

Part of the wedding preparation is a process of meeting with the priest to discuss the Church’s teaching on marriage, but also to discuss some of the more challenging situations that married couples face today. Kelly and Joe spent many nights video conferencing with me about everything from soup to nuts and I have to say they passed with flying colors. I attribute their appreciation for the Church’s understanding of marriage to their families. They saw the beauty of marriage because of their parents, and they desired to have a marriage of their own because they saw the good of marriage in their parents. And St. Thomas Aquinas said the good of marriage is friendship, a life-long partnership where each spouse focuses on what’s best for the other. We talked about how the goal of each spouse is to help one another get to heaven, a focus that is so needed today. And among the ways in which husbands and wives help one another move towards heaven is through prayer. I shared with them the Divine Mercy chaplet, which is a simple devotion to Jesus that can be prayed using Rosary beads. It gave me great joy to know that you were praying together and I hope that you continue to pray the chaplet, making it part of your daily life together.

I’d like to share with you a little inspiration from my own prayers. Yesterday, I was praying my Rosary for you, and on Fridays we meditate on the Sorrowful Mysteries. And while meditating on Jesus’ passion, crucifixion, and death, I have to say, my meditation was anything but sorrowful. In fact, by the time I got to the fourth decade, which is Jesus carrying his cross, I had a most joyful insight. We are often so quick to focus on Jesus’ suffering that we sometimes neglect the reason for which he suffered. True, his carrying of the cross is a symbol of his carrying our broken humanity on his own shoulders; true, his carrying of the cross, is the weight of countless sins; true, his carrying of the cross is a symbol of the weight of the cross we all bear. But, underneath all the muck and grime and sin and pain, Jesus carries our joys, our happiness, our good times, our peace, our love. He carried the cross not only so he could free us from sin and death, but so that his joy might be in us and our joy might be complete. So, in a special way, I want to thank you for the joy of your marriage, a true beacon of hope and love during a time of darkness and pain. May the celebration of your marriage bring you and your families much joy today and throughout your life together.

---
*Given during the COVID-19 pandemic.*